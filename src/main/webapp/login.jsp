<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
    

<c:set var="title" value="Login" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>


<body style="background: #1c5b9e;">
	<div class="container">
	    <div class="row align-items-center justify-content-center">
	    	
	        <div class="col-md-offset-5">
	            <div class="form-login">
	            	<h1 style="color: white;">
	            	<fmt:message key="login_jsp.label.app_name"/>
	            	</h1>
	            	
	            	<form id="login_form" action="controller" method="post">

						<%--=========================================================================== 
						Hidden field. In the query it will act as command=login.
						The purpose of this to define the command name, which have to be executed 
						after you submit current form. 
						===========================================================================--%> 
						<input type="hidden" name="command" value="login"/>
	
						<fieldset >
							<legend style="color: white;">
								<fmt:message key="login_jsp.label.login"/>
							</legend>
							<input name="login"/><br/>
						</fieldset><br/>
						<fieldset>
							<legend style="color: white;">
								<fmt:message key="login_jsp.label.password"/>
							</legend>
							<input type="password" name="password"/>
						</fieldset><br/>
						
						<input class="btn btn-primary" type="submit" value='<fmt:message key="login_jsp.button.login"/>'>								
					</form>
					
	            </div>
	        </div>
	    </div>
	</div>
</body>