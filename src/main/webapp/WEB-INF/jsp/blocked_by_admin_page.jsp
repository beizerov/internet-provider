<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@ include file="/WEB-INF/jspf/head.jspf" %>


<fmt:message key="blocked_by_admin_page_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

	<body>
		<pre id="blocked-by-admin-msg">
			<fmt:message key="blocked_by_admin_page_jsp.message"/>
		</pre>
	</body>
</html>