<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>


<fmt:message key="locale_switch_page_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
	
	<pre id="locale-switch-page-msg">
		<fmt:message key="locale_switch_page_message"/>
	</pre>
	
</body>