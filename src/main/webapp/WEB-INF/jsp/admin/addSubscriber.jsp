<%@page import="java.util.TimerTask"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<%
	// For PRG
	session.setAttribute("command", "goToAddSubscriberPage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>

<fmt:message key="add_subscriber_jsp.title" var="pageTitle" />

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf"%>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>

	<div class="container">
		<h1><fmt:message key="add_subscriber_jsp.label.page_name" /></h1>

		<form id="addSuscriberForm" method="POST" action="controller">
			<input type="hidden" name="command" value="addSuscriber"/>
			<input type="hidden" name="submitted" value="submitted" />
		
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="firstName">
						<fmt:message key="add_subscriber_jsp.label.first_name"/>
					</label> 
						<input type="text" class="form-control" 
							id="firstName" 
							name="firstName"
							placeholder="<fmt:message key="add_subscriber_jsp.label.first_name"/>" 
							pattern="^.{2,25}$"
							title="The first letter is uppercase. Minimum 2 and Maximum 25 letters."
							required>
				</div>
				<div class="form-group col-md-6">
					<label for="lastName">
						<fmt:message key="add_subscriber_jsp.label.last_name"/>
					</label> 
						<input type="text" class="form-control" 
							id="lastName" 
							name="lastName"
							placeholder="<fmt:message key="add_subscriber_jsp.label.last_name"/>" 
							pattern="^.{2,25}$"
							title="The first letter is uppercase. Minimum 2 and Maximum 25 letters."
							required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="login">
						<fmt:message key="add_subscriber_jsp.label.login"/>
					</label> 
						<input type="text" class="form-control"
							id="login" 
							name="login"
							placeholder="<fmt:message key="add_subscriber_jsp.label.login"/>"
							pattern="^(?:\w||\.|!|@){6,25}$"
							title="Login must be between 6 and 25 characters long. Latin letters, numbers, @, ! and period. A period character cannot be first or last."
							required>
				</div>
				<div class="form-group col-md-6">
					<label for="password">
						<fmt:message key="add_subscriber_jsp.label.password"/>
					</label> 
						<input type="password" class="form-control"
							id="password"
							name="password"
							placeholder="<fmt:message key="add_subscriber_jsp.label.password"/>"
							pattern="^.{8,50}$"
							title="Password must be between 8 and 50 characters long. Latin letters, decimal digits zero through nine and characters: ()[]{}/\_-.,:;*!?@#%&quot'&$"
							required>
				</div>
			</div>

			<button type="submit" form="addSuscriberForm" class="btn btn-primary">
				<fmt:message key="add_subscriber_jsp.button.register"/>
			</button>
		</form>
		
		<script type="text/javascript">
			setTimeout(function() {
				$("#successAddedSubscriber").hide();
			}, 1500);
			
		</script>

		<c:if test="${not empty subscriberSuccessMessage}">
			<span id="successAddedSubscriber" class="success">
				${subscriberSuccessMessage}
			</span>
		</c:if>
		
		<span class="error">${sessionScope.subscriberErrorMessage}</span>
	</div>
</body>