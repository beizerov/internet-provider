<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>


<fmt:message key="edit_service_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<h1><fmt:message key="edit_service_jsp.label.page_name" /></h1>

		<form id="serviceEditForm" method="POST" action="controller">
			<input type="hidden" name="command" value="serviceEdit"/>
			<input type="hidden" id="editSubmitted" name="submitted" value="submitted" />
			
			<input type="hidden" name="editServiceId" value="${not empty param.serviceId ? param.serviceId : requestScope.serviceId}"/>
		
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="name">
						<fmt:message key="edit_service_jsp.label.name"/>
					</label> 
						<input type="text" class="form-control" 
							id="editName" 
							name="editServiceName"
							value="${not empty param.serviceName ? param.serviceName : requestScope.serviceName}"
							placeholder="<fmt:message key="edit_service_jsp.label.name"/>" 
							pattern="^.{2,25}$"
							title="Only letters, digits and space. Space can't be the first or last character. Minimum 2 and Maximum 25 letters."
							required>
				</div>
				<div class="form-group col-md-6">
					<label for="price">
						<fmt:message key="edit_service_jsp.label.price"/>
					</label> 
						<input type="number" class="form-control" 
							id="editPrice" 
							name="editServicePrice"
							value="${not empty param.servicePrice ? param.servicePrice : requestScope.servicePrice}"
							placeholder="0.0" 
							min="0"
							max="9999.99"
							step="0.01"
							title="Price is a number in the range 0.0 to 9_999.99"
							required>
				</div>
			</div>

			<button type="submit" form="serviceEditForm" class="btn btn-primary">
				<fmt:message key="edit_service_jsp.button.register"/>
			</button>
		</form>
		
		<c:if test="${not empty serviceErrorMessage}">
			<span class="error">${requestScope.serviceErrorMessage}</span>
		</c:if>
	</div>
</body>