<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%
	// For PRG
	session.setAttribute("command", "goToAdminSubscribersPage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>

<fmt:message key="subscribers_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
	
	<table aria-describedby="subscribers" id="tbl-subscribers" class="table table-striped table-bordered table-sm">
		<thead>
			<tr>
				<th scope="col" class="table-info th-sm"><fmt:message key="subscribers_jsp.table.th.activity"/></th>
				<th scope="col" class="table-info th-sm"><fmt:message key="subscribers_jsp.table.th.login"/></th>
				<th scope="col" class="table-info th-sm"><fmt:message key="subscribers_jsp.table.th.first_name"/></th>
				<th scope="col" class="table-info th-sm"><fmt:message key="subscribers_jsp.table.th.last_name"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="subscriber" items="${requestScope.subscribers}">
				<tr>
					<%@ include file="/WEB-INF/jspf/admin/activitySwitchButton.jspf" %>
				
					<subscriberTagLib:printSubscriber subscriber="${subscriber}" />
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<th scope="col"><fmt:message key="subscribers_jsp.table.th.activity"/></th>
				<th scope="col"><fmt:message key="subscribers_jsp.table.th.login"/></th>
				<th scope="col"><fmt:message key="subscribers_jsp.table.th.first_name"/></th>
				<th scope="col"><fmt:message key="subscribers_jsp.table.th.last_name"/></th>
			</tr>
		</tfoot>
	</table>
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#tbl-subscribers').DataTable();
			$('.dataTables_length').addClass('bs-select');
		});
	</script>
</body>
