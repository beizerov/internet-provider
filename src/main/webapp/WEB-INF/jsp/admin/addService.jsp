<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%
	// For PRG
	session.setAttribute("command", "goToAddServicePage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>

<fmt:message key="add_service_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<h1><fmt:message key="add_service_jsp.label.page_name" /></h1>

		<form id="addServiceForm" method="POST" action="controller">
			<input type="hidden" name="command" value="addService"/>
			<input type="hidden" name="submitted" value="submitted" />
		
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="name">
						<fmt:message key="add_service_jsp.label.name"/>
					</label> 
						<input type="text" class="form-control" 
							id="name" 
							name="name"
							placeholder="<fmt:message key="add_service_jsp.label.name"/>" 
							pattern="^.{2,25}$"
							title="Only letters, digits and space. Space can't be the first or last character. Minimum 2 and Maximum 25 letters."
							required>
				</div>
				<div class="form-group col-md-6">
					<label for="price">
						<fmt:message key="add_service_jsp.label.price"/>
					</label> 
						<input type="number" class="form-control" 
							id="price" 
							name="price"
							placeholder="0.0" 
							min="0"
							max="9999.99"
							step="0.01"
							title="Price is a number in the range 0.0 to 9_999.99"
							required>
				</div>
			</div>

			<button type="submit" form="addServiceForm" class="btn btn-primary">
				<fmt:message key="add_service_jsp.button.register"/>
			</button>
		</form>
		
		<script type="text/javascript">
			setTimeout(function() {
				$("#successAddedService").hide();
			}, 1500);
			
		</script>

		<c:if test="${not empty serviceSuccessMessage}">
			<span id="successAddedService" class="success">
				${serviceSuccessMessage}
			</span>
		</c:if>
		
		<span class="error">${sessionScope.serviceErrorMessage}</span>
	</div>
</body>