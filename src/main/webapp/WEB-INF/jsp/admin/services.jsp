<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%
	// For PRG
	session.setAttribute("command", "goToAdminServicesPage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>

<fmt:message key="services_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
	
	<table aria-describedby="services" id="tbl-services" class="table table-striped table-bordered table-sm">
		<thead>
			<tr>
				<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.name"/></th>
				<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.price"/></th>
				<th scope="col"></th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="service" items="${requestScope.services}">
				<tr>
					<serviceTagLib:printService service="${service}" />
					
					<%@ include file="/WEB-INF/jspf/admin/serviceEditButton.jspf" %>
					
					<%@ include file="/WEB-INF/jspf/admin/serviceDeleteButton.jspf" %>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<th scope="col"><fmt:message key="services_jsp.table.th.name"/></th>
				<th scope="col"><fmt:message key="services_jsp.table.th.price"/></th>
				<th scope="col"></th>
				<th scope="col"></th>
			</tr>
		</tfoot>
	</table>
	
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#tbl-services').DataTable();
			$('.dataTables_length').addClass('bs-select');
		});
	</script>
</body>
