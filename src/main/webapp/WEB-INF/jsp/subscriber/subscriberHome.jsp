<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>


<%
	// For PRG
	session.setAttribute("command", "goToSubscriberHomePage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>


<fmt:message key="home_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
	
	<h2><fmt:message key="subscriber_home_jsp.account_number"/>:  ${requestScope.subscriberAccount.accountNumber}"</h2>
	<h2><fmt:message key="subscriber_home_jsp.balance"/>: ${requestScope.subscriberAccount.balance}"</h2>

	
	<table aria-describedby="services" id="tbl-services" class="table table-striped table-bordered table-sm">
		<caption><fmt:message key="subscriber_home_jsp.table.name"/></caption>
		<thead>
			<tr>
				<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.name"/></th>
				<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.price"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="service" items="${requestScope.subscriberServices}">
				<tr>
					<serviceTagLib:printService service="${service}" />
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<th scope="col"><fmt:message key="services_jsp.table.th.name"/></th>
				<th scope="col"><fmt:message key="services_jsp.table.th.price"/></th>
			</tr>
		</tfoot>
	</table>
</body>
