<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%
	// For PRG
	session.setAttribute("command", "goToAddServicesPage");

	if (request.getParameter("submitted")  != null) {
		response.sendRedirect(request.getContextPath() + "/controller");
	}
%>

<fmt:message key="services_jsp.title" var="pageTitle"/>

<c:set var="title" value="${pageTitle}" scope="page" />

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
	
	<input type="button" class="btn-danger" id="btnPdfExport" value="Export as pdf" />
	
	<form action="controller" method="POST" id="AddServicesForm">
		<input 
			type="hidden" 
			id="makeAnOrder$" 
			name="command"
			value="makeAnOrder"/>
			
		<input 
			type="hidden" 
			name="submitted" 
			value="submitted" />
	
		<table aria-describedby="services" id="tbl-services" class="table table-striped table-bordered table-sm">
			<thead>
				<tr>
					<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.name"/></th>
					<th scope="col" class="table-info th-sm"><fmt:message key="services_jsp.table.th.price"/></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="service" items="${requestScope.services}">
					<tr>
						<serviceTagLib:printService service="${service}" />
						<td>
							<input type="checkbox" name="serviceId" value="${service.id}">
						</td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<th scope="col"><fmt:message key="services_jsp.table.th.name"/></th>
					<th scope="col"><fmt:message key="services_jsp.table.th.price"/></th>
					<th scope="col"></th>
				</tr>
			</tfoot>
		</table>
		
		<button id="go-to-add-servs-btn" class="btn btn-primary" type="submit" form="AddServicesForm" value="Submit">
			<fmt:message key="add_services_jsp.button"/>
		</button>
	</form>
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#tbl-services').DataTable();
			$('.dataTables_length').addClass('bs-select');
		});
		
        $("body").on("click", "#btnPdfExport", function () {
            html2canvas($('#tbl-services')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download(
                    	"internet-provider-services-prices.pdf"
                    );
                }
            });
        });
	</script>
</body>
