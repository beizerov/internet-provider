package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class LocaleSetCommand<T extends UserDao> extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			LocaleSetCommand.class
	);
	
	private final T userDao;
	
	
	public LocaleSetCommand(T userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException(
				"userDao argument can't be null"
			);
		}
		
		this.userDao = userDao;
	}
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("LocaleSetCommand starts");
		
		String localeToSet = request.getParameter("localeToSet");
		
		if (localeToSet != null && !localeToSet.isEmpty()) {
			try {
				HttpSession httpSession = request.getSession();
				
				User user = (User) httpSession.getAttribute("user");
				
				user.setLocale(localeToSet);
				
				Config.set(
					httpSession,
					"javax.servlet.jsp.jstl.fmt.locale",
					localeToSet
				);
				
				httpSession.setAttribute("defaultLocale", localeToSet);
				
				userDao.setLocale(user);
				
				LOGGER.debug("Set " + localeToSet + " locale");
			} catch (InvalidDataException e) {
				LOGGER.warn(
					CommandContainer.ERROR_MESSAGE 
					+ " ::Locale not set:: " + e.getMessage() 
				);
			} catch (DatabaseException e) {
				LOGGER.error(
					CommandContainer.ERROR_MESSAGE + e.getMessage(), e
				);
			}
		}
		
		
		LOGGER.trace("LocaleSetCommand starts finished");
		
		return Path.PAGE_LOCALE_SWITCH;
	}

}
