package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.Path;


public class SubscriberRegistrationPageCommand extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			SubscriberRegistrationPageCommand.class
	);
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("SubscriberRegistrationPageCommand starts");
		LOGGER.debug("forward to " + Path.PAGE_ADD_SUBSCRIBER);
		LOGGER.trace("SubscriberRegistrationPageCommand finished");
		
		return Path.PAGE_ADD_SUBSCRIBER;
	}
}
