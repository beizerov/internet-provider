package ua.nure.beizerov.iprovider.web.controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.command.Command;
import ua.nure.beizerov.iprovider.web.command.NoCommand;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


/**
 * Main servlet controller.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
@WebServlet("/controller")
public class InternetProviderController extends HttpServlet {

	private static final String COMMAND = "command";

	private static final long serialVersionUID = 995838651983545653L;
	
	private static final Logger LOGGER = Logger.getLogger(
			InternetProviderController.class
	);

	
	/**
	 * @see HttpServlet#doGet(
	 *          HttpServletRequest request, HttpServletResponse response
	 *      )
	 */
	@Override
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response
	) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (Exception e) {
			LOGGER.error("Error in InternetProviderController.class => ", e);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(
	 *          HttpServletRequest request, HttpServletResponse response
	 *      )
	 */
	@Override
	protected void doPost(
			HttpServletRequest request, HttpServletResponse response
	) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (Exception e) {
			LOGGER.error("Error in InternetProviderController.class => ", e);
		}
	}
	
	
	/**
	 * Main method of this controller. 
	 */
	private void process(
			HttpServletRequest request, HttpServletResponse response
	) throws ServletException, IOException {
		LOGGER.trace("Controller starts");

		// extract command name from the request
		String commandName = request.getParameter(COMMAND);
		LOGGER.debug("Request parameter: command --> " + commandName);

		// obtain command object by its name
		Command command = CommandContainer.get(commandName);
		
		//  ===================== For PRG =====================
		HttpSession session = request.getSession(false);
		if (session != null) {
			String prgCommandName = (String) session.getAttribute(COMMAND);
			
			if (command instanceof NoCommand && prgCommandName != null) {
				command = CommandContainer.get(prgCommandName);
				
				session.setAttribute(COMMAND, null);
				
				LOGGER.debug("REDIRECT--> " + command);
			}
		}
		//  ====================================================
		
		LOGGER.debug("Obtained command --> " + command);

		// execute command and get forward address
		String forward = command.execute(request, response);
		LOGGER.debug("Forward address --> " + forward);

		LOGGER.debug(
			"Controller finished, now go to forward address --> " + forward
		);

		// if the forward address is not null go to the address
		if (forward != null) {
			RequestDispatcher disp = request.getRequestDispatcher(forward);
			disp.forward(request, response);
		}
	}
}
