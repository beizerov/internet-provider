package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.Path;


public class LogoutCommand extends Command {

	private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("LogoutCommand starts");
		
		HttpSession session = request.getSession(false);
		
		if (session != null)
			session.invalidate();

		LOGGER.trace("LogoutCommand finished");
		
		return Path.PAGE_LOGIN;
	}
}
