package ua.nure.beizerov.iprovider.web.filter;


import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.apache.log4j.Logger;


/**
 * This filter makes sure that if the browser hasn't set the encoding
 * used in the request, that it's set to UTF-8.
 * 
 * 
 * Servlet Filter implementation class CharsetFilter
 */
@WebFilter("/*")
public class CharsetFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(CharsetFilter.class);
	
	
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(
			ServletRequest request, 
			ServletResponse response, 
			FilterChain chain
	) throws IOException, ServletException {
        // Respect the client-specified character encoding
        // (see HTTP specification section 3.4.1)
        if (null == request.getCharacterEncoding()) {
            request.setCharacterEncoding("UTF-8");
            
            LOGGER.trace("Set the encoding UTF-8");
        }

		chain.doFilter(request, response);
	}
}
