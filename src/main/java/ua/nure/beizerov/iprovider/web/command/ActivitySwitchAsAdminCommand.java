package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.web.Path;


public class ActivitySwitchAsAdminCommand<T extends UserDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			ActivitySwitchAsAdminCommand.class
	);
	
	private final T userDao;
	
	
	public ActivitySwitchAsAdminCommand(T userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException(
				"userDao argument can't be null"
			);
		}
		
		this.userDao = userDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("ActivitySwitchCommand starts");
		
		String subscriberId = request.getParameter("subscriberId");
		
		if (subscriberId != null) {
			try {
				Optional<User> subcriber = userDao.findById(
					Long.parseLong(subscriberId)
				);
				
				subcriber.ifPresent(s -> {
					try {
						userDao.switchActivityAsAdmin(s);
					} catch (DatabaseException e) {
						LOGGER.error(e.getMessage(), e);
					}
					
					LOGGER.debug(
						"Subscriber with id = " + subscriberId + " was blocked"
					);
				});
				
			} catch (NumberFormatException | DatabaseException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		
		LOGGER.trace("ActivitySwitchCommand finished");
		return Path.PAGE_ADMIN_SUBSCRIBERS;
	}
}
