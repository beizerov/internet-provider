package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DaoException;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class SubscriberRegistrationCommand<T extends UserDao>  extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			SubscriberRegistrationCommand.class
	);
	
	private final T userDao;
	
	
	public SubscriberRegistrationCommand(T userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException(
				"userDao argument can't be null"
			);
		}
		
		this.userDao = userDao;
	}

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		LOGGER.trace("SubscriberRegistrationCommand starts");
		
		User authorizedUser = (User) request.getSession(false).getAttribute("user");
		
		String locale = authorizedUser.getLocale();
		String defaultLocale = request.getServletContext().getInitParameter(
			"javax.servlet.jsp.jstl.fmt.locale"
		);
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
			CommandContainer.BASE_NAME, new Locale(
				(locale != null) ? locale : defaultLocale
			)
		);
		
		// error handler
		String errorMessage = null;
		
		User user = new User();
		
		try {
			final String login = request.getParameter("login");
			
			if (isNotPressent(login)) {
				user.setFirstName(request.getParameter("firstName"));
				user.setLastName(request.getParameter("lastName"));
				user.setLogin(login);
				user.setPassword(request.getParameter("password"));
				
				LOGGER.debug("Execute add subscriber => " + user);
				
				userDao.createSubscriber(user);
				
				// Remove error message (errorMessage initialized null)
				addToSessionErrorMessageAttribute(
					request, errorMessage
				);
				
				removeSuccessMessageAttributeFromSession(request);
				
				request
					.getSession(false)
					.setAttribute(
						"subscriberSuccessMessage",
							resourceBundle.getString(
								"add_subscriber_jsp.success_message"
							)
					)
				;
				
				LOGGER.trace("Subscriber was added");
			} else {
				String messagePart1 = resourceBundle.getString(
					"add_subscriber_jsp.error_message_subscriber_already_registered_part_1"
				);
				
				String messagePart2 = resourceBundle.getString(
					"add_subscriber_jsp.error_message_subscriber_already_registered_part_2"
				);
				
				errorMessage = messagePart1 + " " + login + " " + messagePart2;
				
				addToSessionErrorMessageAttribute(
					request, errorMessage
				);
				
				LOGGER.warn(errorMessage);
			}
		} catch (InvalidDataException e) {
			errorMessage = e.getMessage();
			
			addToSessionErrorMessageAttribute(
				request, errorMessage
			);
			
			LOGGER.warn(
				CommandContainer.ERROR_MESSAGE
				+ errorMessage + " " + user
			);
		} catch (DaoException | DatabaseException e) {
			errorMessage = e.getMessage();
			
			LOGGER.error(CommandContainer.ERROR_MESSAGE + errorMessage, e);
		}
		
		LOGGER.trace("SubscriberRegistrationCommand finished");
		
		return Path.PAGE_ADD_SUBSCRIBER;
	}


	private boolean isNotPressent(final String login)
			throws DaoException, DatabaseException {
		return !userDao.findByLogin(login).isPresent();
	}
	
	private void addToSessionErrorMessageAttribute(
			HttpServletRequest request, String errorMessage
	) {
		HttpSession session = request.getSession(false);
		
		session.setAttribute("subscriberErrorMessage", errorMessage);
	}
	
	private void removeSuccessMessageAttributeFromSession(
			HttpServletRequest request
	) {
		Timer timer = new Timer();

		final HttpSession session = request.getSession(false);
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				session.setAttribute("subscriberSuccessMessage", null);
			}
		}, 1000L * 1L);
	}
}
