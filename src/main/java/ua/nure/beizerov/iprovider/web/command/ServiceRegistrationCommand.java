package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Service;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class ServiceRegistrationCommand<T extends ServiceDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			ServiceRegistrationCommand.class
	);
	
	private final T serviceDao;
	

	public ServiceRegistrationCommand(T serviceDao) {
		if (serviceDao == null) {
			throw new IllegalArgumentException(
				"serviceDao argument can't be null"
			);
		}
		
		this.serviceDao = serviceDao;
	}
	
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		LOGGER.trace("ServiceRegistrationCommand starts");

		User authorizedUser = (User) request.getSession(false).getAttribute("user");
		
		String locale = authorizedUser.getLocale();
		String defaultLocale = request.getServletContext().getInitParameter(
			"javax.servlet.jsp.jstl.fmt.locale"
		);
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
			CommandContainer.BASE_NAME, new Locale(
				(locale != null) ? locale : defaultLocale
			)
		);
		
		// error handler
		String errorMessage = null;		
		
		Service service = new Service();
		
		try {
			final String serviceName = request.getParameter("name");
			
			if (isNotPressent(serviceName)) {
				service.setName(serviceName);
				service.setPrice(
					Float.parseFloat(request.getParameter("price"))
				);
				
				serviceDao.create(service);
				
				// Remove error message (errorMessage initialized null)
				addToSessionErrorMessageAttribute(
					request, errorMessage
				);
				
				removeSuccessMessageAttributeFromSession(request);
				
				request
					.getSession(false)
					.setAttribute(
						"serviceSuccessMessage",
							resourceBundle.getString(
								"add_service_jsp.success_message"
							)
					)
				;
			
				LOGGER.debug("Add service => " + service);
				LOGGER.trace("Service was added");
			} else {
				String messagePart1 = resourceBundle.getString(
					"add_service_jsp.error_message_service_already_registered_part_1"
				);
					
				String messagePart2 = resourceBundle.getString(
					"add_service_jsp.error_message_service_already_registered_part_2"
				);
				
				errorMessage = messagePart1 + " " + serviceName + " " + messagePart2;
				
				addToSessionErrorMessageAttribute(
					request, errorMessage
				);
				
				LOGGER.warn(errorMessage);
			}
		} catch (InvalidDataException e) {
			errorMessage = e.getMessage();
			
			addToSessionErrorMessageAttribute(
				request, errorMessage
			);
			
			LOGGER.warn(
				CommandContainer.ERROR_MESSAGE
				+ errorMessage + " " + service
			);
		} catch (DatabaseException e) {
			errorMessage = e.getMessage();
			
			LOGGER.error(CommandContainer.ERROR_MESSAGE + errorMessage, e);
		}
		
		LOGGER.trace("ServiceRegistrationCommand finished");
		
		return Path.PAGE_ADD_SERVICE;
	}
	
	
	private boolean isNotPressent(final String serviceName)
			throws DatabaseException {
		return !serviceDao.findByName(serviceName).isPresent();
	}
	
	private void addToSessionErrorMessageAttribute(
			HttpServletRequest request, String errorMessage
	) {
		HttpSession session = request.getSession(false);
		
		session.setAttribute("serviceErrorMessage", errorMessage);
	}
	
	private void removeSuccessMessageAttributeFromSession(
			HttpServletRequest request
	) {
		Timer timer = new Timer();

		final HttpSession session = request.getSession(false);
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				session.setAttribute("serviceSuccessMessage", null);
			}
		}, 1000L * 1L);
	}
}
