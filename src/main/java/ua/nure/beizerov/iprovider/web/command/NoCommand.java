package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.Path;


public class NoCommand extends Command {

	private static final Logger LOGGER = Logger.getLogger(NoCommand.class); 
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("NoCommand starts");
		
		String errorMessage = "No such command";
		
		request.setAttribute("errorMessage", errorMessage);
		
		LOGGER.error(
			"Set the request attribute: errorMessage --> " + errorMessage
		);

		LOGGER.trace("NoCommand finished");
		
		return Path.PAGE_ERROR_PAGE;
	}
}
