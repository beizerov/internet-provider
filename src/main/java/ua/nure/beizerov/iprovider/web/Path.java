package ua.nure.beizerov.iprovider.web;


/**
 * Path holder (jsp pages, controller commands).
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public final class Path {

	private Path() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	
	public static final 
	String PAGE_LOGIN = "/login.jsp";
	
	public static final 
	String PAGE_LOCALE_SWITCH = "/WEB-INF/jsp/localeSwitchPage.jsp";
	
	public static final
	String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
	
	public static final
	String PAGE_ACCOUNT_BLOCKED_BY_ADMIN = "/WEB-INF/jsp/blocked_by_admin_page.jsp";
	
	public static final
	String PAGE_TOP_UP_BALANCE = "/WEB-INF/jsp/top_up_balance_page.jsp";

	
	// admin pages
	public static final
	String PAGE_ADMIN_WELCOME = "/WEB-INF/jsp/admin/welcomePage.jsp";
	
	public static final
	String PAGE_ADMIN_SUBSCRIBERS = "/WEB-INF/jsp/admin/subscribers.jsp";
	
	public static final
	String PAGE_ADMIN_SERVICES = "/WEB-INF/jsp/admin/services.jsp";
	
	public static final
	String PAGE_ADD_SUBSCRIBER = "/WEB-INF/jsp/admin/addSubscriber.jsp";
	
	public static final
	String PAGE_ADD_SERVICE = "/WEB-INF/jsp/admin/addService.jsp";
	
	public static final
	String PAGE_EDIT_SERVICE = "/WEB-INF/jsp/admin/editService.jsp";
	
	
	// subscriber pages
	public static final
	String PAGE_SUBSCRIBER_HOME = "/WEB-INF/jsp/subscriber/subscriberHome.jsp";
	
	public static final
	String PAGE_ADD_SERVICES = "/WEB-INF/jsp/subscriber/addServices.jsp";
}
