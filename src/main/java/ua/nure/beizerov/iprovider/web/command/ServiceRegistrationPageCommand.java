package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.Path;


public class ServiceRegistrationPageCommand extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			ServiceRegistrationPageCommand.class
	);
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("ServiceRegistrationPageCommand starts");
		LOGGER.debug("forward to " + Path.PAGE_ADD_SERVICE);
		LOGGER.trace("ServiceRegistrationPageCommand finished");
		
		return Path.PAGE_ADD_SERVICE;
	}
}
