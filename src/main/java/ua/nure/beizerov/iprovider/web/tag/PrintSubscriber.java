package ua.nure.beizerov.iprovider.web.tag;


import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.User;


public class PrintSubscriber extends TagSupport {

	private static final long serialVersionUID = -2087710035595401977L;

	private static final Logger LOGGER = Logger.getLogger(PrintSubscriber.class);

	private User subscriber;

	
	public void setSubscriber(User subscriber) {
		this.subscriber = subscriber;
	}


	@Override
	public int doStartTag() throws JspException {
		StringBuilder out = new StringBuilder();
		
		final String startTagTd = "<td>"; 
		final String endTagTd = "</td>";
		
		out
			.append(startTagTd)
			.append(subscriber.getLogin())
			.append(endTagTd)
			.append(startTagTd)
			.append(subscriber.getFirstName())
			.append(endTagTd)
			.append(startTagTd)
			.append(subscriber.getLastName())
			.append(endTagTd)
		;
		
		try {
			pageContext.getOut().println(out);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		LOGGER.debug(subscriber);
		
		return super.doStartTag();
	}
}
