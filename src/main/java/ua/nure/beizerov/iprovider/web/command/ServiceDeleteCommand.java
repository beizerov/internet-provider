package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.web.Path;


public class ServiceDeleteCommand<T extends ServiceDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			ServiceDeleteCommand.class
	);
	
	private final T serviceDao;
	
	
	public ServiceDeleteCommand(T serviceDao) {
		if (serviceDao == null) {
			throw new IllegalArgumentException(
				"serviceDao argument can't be null"
			);
		}
		
		this.serviceDao = serviceDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("ServiceDeleteCommand starts");
		
		String serviceId = request.getParameter("serviceId");
		
		if (serviceId != null) {
			try {
				serviceDao.delete(Long.parseLong(serviceId));
				
				LOGGER.debug("Service with " + serviceId + "was deleted");
			} catch (NumberFormatException | DatabaseException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		
		LOGGER.trace("ServiceDeleteCommand finished");
		return Path.PAGE_ADMIN_SERVICES;
	}
}
