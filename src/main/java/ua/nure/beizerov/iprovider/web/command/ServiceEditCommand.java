package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Service;
import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class ServiceEditCommand<T extends ServiceDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			ServiceEditCommand.class
	);
	
	private final T serviceDao;
	
	
	public ServiceEditCommand(T serviceDao) {
		if (serviceDao == null) {
			throw new IllegalArgumentException(
				"serviceDao argument can't be null"
			);
		}
		
		this.serviceDao = serviceDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("ServiceEditCommand starts");
		
		// error handler
		String errorMessage = null;
		
		String forward = Path.PAGE_EDIT_SERVICE;
		
		Service service = null;
		
		String serviceId = request.getParameter("editServiceId");
		String serviceName = request.getParameter("editServiceName");
		String servicePrice = request.getParameter("editServicePrice");
		
		if (serviceId != null && serviceName != null && servicePrice != null) {
			try {
				service = serviceDao.findById(
					Long.parseLong(serviceId)
				).orElseThrow();				
				
				LOGGER.debug("Edit service from => " + service);
				
				float sPrice = Float.parseFloat(servicePrice);
				
				if (
					service.getName().equals(serviceName) 
					&& 
					service.getPrice() == sPrice
				) {
					LOGGER.debug(
						"The data remained the same. "
						+ "Because the input hasn't changed for => " + service
					);
				} else {
					service.setName(serviceName);
					service.setPrice(sPrice);
				
					serviceDao.update(service);
				}
				
				// Remove error message (errorMessage initialized null)
				addToRequestErrorMessageAttribute(request, errorMessage);
				
				LOGGER.debug("Edit service to => " + service);
				
				forward = Path.PAGE_ADMIN_SERVICES;
			}  catch (InvalidDataException e) {
				errorMessage = e.getMessage();
				
				addToRequestErrorMessageAttribute(
					request, errorMessage
				);
				
				request.setAttribute("serviceId", serviceId);
				request.setAttribute("serviceName", serviceName);
				request.setAttribute("servicePrice", servicePrice);
				
				LOGGER.warn(
					CommandContainer.ERROR_MESSAGE
					+ errorMessage + " " + serviceName
				);
			} catch (DatabaseException | NoSuchElementException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		
		LOGGER.trace("ServiceEditCommand finished");
		return forward;
	}
	
	private void addToRequestErrorMessageAttribute(
			HttpServletRequest request, String errorMessage
	) {
		request.setAttribute("serviceErrorMessage", errorMessage);
	}
}
