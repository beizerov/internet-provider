package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.web.Path;


public class ServiceEditPageCommand extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			ServiceEditPageCommand.class
	);
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("ServiceEditPageCommand starts");
		
		LOGGER.debug("forward to " + Path.PAGE_EDIT_SERVICE);
		LOGGER.trace("ServiceEditPageCommand finished");
		
		return Path.PAGE_EDIT_SERVICE;
	}
}
