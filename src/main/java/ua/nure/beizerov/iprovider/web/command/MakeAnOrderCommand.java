package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.Arrays;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.AccountDao;
import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class MakeAnOrderCommand
	<A extends AccountDao, S extends ServiceDao, U extends UserDao> 
	extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			MakeAnOrderCommand.class
	);
	
	private final A accountDao;
	private final S serviceDao;
	private final U userDao;
	

	public MakeAnOrderCommand(A accountDao, S serviceDao, U userDao) {
		if (accountDao == null || serviceDao == null || userDao == null) {
			throw new IllegalArgumentException(
				"arguments can't be null"
			);
		}
		
		this.accountDao = accountDao;
		this.serviceDao = serviceDao;
		this.userDao = userDao;
	}
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("MakeAnOrderCommand starts");
		
		String[] arrayOfServiceIdStr = request.getParameterValues("serviceId");
	
		String forward = Path.PAGE_SUBSCRIBER_HOME;
		
		if (arrayOfServiceIdStr != null && arrayOfServiceIdStr.length != 0) {
			long[] arrayOfServiceId = convertNumberInStringRepresentationToLon(
					arrayOfServiceIdStr
			);
			
			User subscriber = (User) request.getSession().getAttribute("user");

			try {
				Account account = accountDao.findAccountByUserId(
					subscriber.getId()
				).orElseThrow();
				
				accountDao.addSevicesToAccountById(
					account.getId(), arrayOfServiceId
				);
				
				double balance = account.getBalance();
				
				balance -= calculateOrderAmount(arrayOfServiceId);
				
				account.setBalance((float) balance);
				
				accountDao.updateBalance(account);
				
				if (balance < 0) {
					forward = Path.PAGE_TOP_UP_BALANCE;
					
					userDao.switchActivity(subscriber);
					
					LOGGER.debug(
						"Subscriber " + subscriber + "was blocked"
					);
				}
				
				LOGGER.debug(
					"Add services with ids " 
					+ Arrays.toString(arrayOfServiceId)
				);
			} catch (
					DatabaseException 
					| 
					NoSuchElementException 
					| 
					InvalidDataException e
			) {
				LOGGER.error(CommandContainer.ERROR_MESSAGE + e.getMessage());
			}
		}
		
		LOGGER.debug("forward to " + Path.PAGE_ADD_SERVICES);
		LOGGER.trace("MakeAnOrderCommand finished");
		
		return forward;
	}
	
	
	private static long[] convertNumberInStringRepresentationToLon(
			final String[] numberInStringRepresentation
	) {
		if (
			numberInStringRepresentation != null
			&& 
			numberInStringRepresentation.length != 0
		) {
			final long[] numbers = new long[numberInStringRepresentation.length]; 
			
			for (int i = 0; i < numbers.length; i++) {
				numbers[i] = Long.parseLong(numberInStringRepresentation[i]);
			}
			
			return numbers;
		} else {
			throw new IllegalArgumentException(
				"numberInStringRepresentation parameter can't be null"
			);
		}
	}
	
	private float calculateOrderAmount(long[] arrayOfServiceId) 
			throws DatabaseException {
		double orderAmount = 0.0;
		
		for (long id : arrayOfServiceId) {
			
			orderAmount += serviceDao.findById(id).orElseThrow().getPrice();
		}
		
		return (float) orderAmount;
	}
}
