package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Role;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DaoException;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class LoginCommand<T extends UserDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

	private final T userDao;
	
	
	public LoginCommand(T userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException(
				"userDao argument can't be null"
			);
		}
		
		this.userDao = userDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("LoginCommand execution");
		
		HttpSession httpSession = request.getSession();
		
		// obtain login and password from the request
		String login = request.getParameter("login");
		LOGGER.debug("Request parameter: loging --> " + login);
		
		String password = request.getParameter("password");
		
		// error handler
		String errorMessage = null;		
		String forward = Path.PAGE_ERROR_PAGE;
		
		if (
				login == null 
			|| 
				password == null
			|| 
				login.isEmpty() 
			|| 
				password.isEmpty()
		) {
			errorMessage = "Login/password cannot be empty";
			
			addToRequestErrorMessageAttribute(request, errorMessage);
			
			LOGGER.error(CommandContainer.ERROR_MESSAGE + errorMessage);
			
			return forward;
		}
		
		User user = null;
		
		try {
			user = userDao.findByLogin(login).orElseThrow();
		} catch (DaoException | DatabaseException | NoSuchElementException e) {
			LOGGER.error(InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e);
			
			errorMessage = InvalidDataExceptionMessages.FAILED_TO_FIND_USERS;
			
			addToRequestErrorMessageAttribute(request, errorMessage);
			
			LOGGER.error(CommandContainer.ERROR_MESSAGE + errorMessage);
			
			return forward;
		}
		
		LOGGER.debug("Found in DB: user --> " + user);
			
		if (!password.equals(user.getPassword())) {
			errorMessage = "Cannot find user with such login/password";
			
			addToRequestErrorMessageAttribute(request, errorMessage);
			
			LOGGER.error(CommandContainer.ERROR_MESSAGE + errorMessage);
			
			return forward;
		} else {
			Role userRole = user.getRole();
			
			LOGGER.debug("userRole --> " + userRole);
				
			switch (userRole) {
				case ADM:
					forward = Path.PAGE_ADMIN_WELCOME;
					
					break;
				case SUB:
				forward = routeSubscriber(user);
					
					break;
				default:
					forward = Path.PAGE_ERROR_PAGE;
					break;
			}
			
			httpSession.setAttribute("user", user);
			LOGGER.debug("Set the session attribute: user --> " + user);
				
			httpSession.setAttribute("userRole", userRole);				
			LOGGER.debug("Set the session attribute: userRole --> " + userRole);
				
			LOGGER.info(
				"User " + user.getLogin() + " logged as " + userRole
			);
			
			// work with i18n
			String userLocale = user.getLocale();
			LOGGER.debug("userLocalName --> " + userLocale);
			
			if (userLocale != null && !userLocale.isEmpty()) {
				Config.set(
					httpSession,
					"javax.servlet.jsp.jstl.fmt.locale",
					userLocale
				);
				
				httpSession.setAttribute(
					"defaultLocale", userLocale
				);
				
				LOGGER.debug(
					"Set the session attribute: defaultLocaleName --> " 
					+ userLocale
				);
				
				LOGGER.info(
					"Locale for user: defaultLocale --> " + userLocale
				);
			}
		}
		
		LOGGER.trace("LoginCommand finished");
		
		return forward;
	}


	private String routeSubscriber(User subscriber) {
		String forward = null;
		
		if (subscriber.isActive()) {
			forward = Path.PAGE_SUBSCRIBER_HOME;
		} else {
			if (subscriber.getActive() == -1) {
				forward = Path.PAGE_ACCOUNT_BLOCKED_BY_ADMIN;
			} else {
				forward = Path.PAGE_TOP_UP_BALANCE;
			}
		}
		return forward;
	}
	
	
	private void addToRequestErrorMessageAttribute(
			HttpServletRequest request, String errorMessage
	) {
		request.setAttribute("errorMessage", errorMessage);
	}
}
