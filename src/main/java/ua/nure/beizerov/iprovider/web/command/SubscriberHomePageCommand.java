package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.dao.AccountDao;
import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class SubscriberHomePageCommand
	<S extends ServiceDao, A extends AccountDao> extends Command {
	
	private static final Logger LOGGER = Logger.getLogger(
			SubscriberHomePageCommand.class
	);

	private final A accountDao;
	private final S serviceDao;
	
	
	public SubscriberHomePageCommand(A accountDao, S serviceDao) {
		if (accountDao == null || serviceDao == null) {
			throw new IllegalArgumentException(
				"arguments can't be null"
			);
		}
		
		this.accountDao = accountDao;
		this.serviceDao = serviceDao;
	}
	

	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response
	) throws IOException, ServletException {
		LOGGER.trace("SubscriberHomePageCommand starts");
		
		try {
			HttpSession session = request.getSession();
			
			Account account = accountDao.findAccountByUserId(
					((User) session.getAttribute("user")).getId())
					.orElseThrow()
			;
			
			request.setAttribute("subscriberAccount", account);
			
			request.setAttribute(
				"subscriberServices", serviceDao.findServicesByUserId(
					((User) session.getAttribute("user")).getId()
			));
		} catch (DatabaseException e) {
			LOGGER.error(CommandContainer.ERROR_MESSAGE + e.getMessage());
		}
		
		LOGGER.debug("forward to " + Path.PAGE_SUBSCRIBER_HOME);
		LOGGER.trace("SubscriberHomePageCommand finished");
		
		return Path.PAGE_SUBSCRIBER_HOME;
	}
}
