package ua.nure.beizerov.iprovider.web.listener;


import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.exception.ApplicationRuntimeException;


/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {
	private static final Logger LOGGER = Logger.getLogger(ContextListener.class);
	
	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		log("contextInitialized");
		
		ServletContext servletContext = event.getServletContext();
		
		initCommandContainer();
		initI18N(servletContext);
		
		log("Log4J initialization finished");
	}

	
	/**
	 * Initializes CommandContainer.
	 * 
	 * @param servletContext
	 */
	private void initCommandContainer() {
		log("Command container initialization started");
		
		// initialize commands container
		// just load class to JVM
		try {
			Class.forName(
				"ua.nure.beizerov.iprovider.web.command.util.CommandContainer"
			);
		} catch (ClassNotFoundException e) {
			LOGGER.error(
				"ua.nure.beizerov.iprovider.web.command.util.CommandContainer"
				+ " ==> Class not found"
			);
			
			throw new ApplicationRuntimeException();
		}
		
		log("Command container initialization finished");
	}
	
	/**
	 * Initializes i18n subsystem.
	 */
	private void initI18N(ServletContext servletContext) {
		log("I18N subsystem initialization started");
		
		String localesValue = servletContext.getInitParameter("locales");
		if (localesValue == null || localesValue.isEmpty()) {
			LOGGER.warn(
				"'locales' init parameter is empty,"
				+ " the default encoding will be used"
			);
		} else {
			List<String> locales = new ArrayList<>();
			StringTokenizer st = new StringTokenizer(localesValue);
			
			while (st.hasMoreTokens()) {
				String localeName = st.nextToken();
				locales.add(localeName);
			}							
			
			LOGGER.debug("Application attribute set: locales --> " + locales);
			
			servletContext.setAttribute("locales", locales);
		}		
		
		log("I18N subsystem initialization finished");
	}
	
	
	private void log(String msg) {
		LOGGER.trace("[ContextListener] " + msg);
	}
}
