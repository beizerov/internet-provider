package ua.nure.beizerov.iprovider.web.tag;


import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Service;


public class PrintService extends TagSupport {

	private static final long serialVersionUID = -2038498464990015261L;

	private static final Logger LOGGER = Logger.getLogger(PrintService.class);

	private Service service;

	
	public void setService(Service service) {
		this.service = service;
	}


	@Override
	public int doStartTag() throws JspException {
		StringBuilder out = new StringBuilder();
		
		final String startTagTd = "<td>"; 
		final String endTagTd = "</td>";
		
		out
			.append(startTagTd)
			.append(service.getName())
			.append(endTagTd)
			.append(startTagTd)
			.append(service.getPrice())
			.append(endTagTd)
		;
		
		try {
			pageContext.getOut().println(out);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		LOGGER.debug(service);
		
		return super.doStartTag();
	}
}
