package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Main interface for the Command pattern implementation.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public abstract class Command {

	/**
	 * Execution method for command.
	 * 
	 * @param request
	 * @param response
	 * @return Address to go once the command is executed.
	 * @throws IOException
	 * @throws ServletException
	 */
	public abstract String execute(
			HttpServletRequest request, HttpServletResponse response
	) throws IOException, ServletException;
	
	@Override
	public final String toString() {
		return getClass().getSimpleName();
	}
}
