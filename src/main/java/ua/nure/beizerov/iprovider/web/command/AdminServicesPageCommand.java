package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class AdminServicesPageCommand<T extends ServiceDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			AdminServicesPageCommand.class
	);
	
	private final T serviceDao;

	
	public AdminServicesPageCommand(T serviceDao) {
		if (serviceDao == null) {
			throw new IllegalArgumentException(
				"serviceDao argument can't be null"
			);
		}
		
		this.serviceDao = serviceDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		LOGGER.trace("AdminServicesPageCommand starts");
		
		try {
			request.setAttribute("services", serviceDao.findAll());
		} catch (DatabaseException e) {
			LOGGER.error(CommandContainer.ERROR_MESSAGE + e.getMessage(), e);
		}
		
		LOGGER.debug("forward to " + Path.PAGE_ADMIN_SERVICES);
		LOGGER.trace("AdminServicesPageCommand finished");
		
		return Path.PAGE_ADMIN_SERVICES;
	}
}
