package ua.nure.beizerov.iprovider.web.command.util;


import java.util.Map;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.persistence.dao.AccountDaoRdbms;
import ua.nure.beizerov.iprovider.model.persistence.dao.ServiceDaoRdbms;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDaoRdbms;
import ua.nure.beizerov.iprovider.web.command.ActivitySwitchAsAdminCommand;
import ua.nure.beizerov.iprovider.web.command.AddServicesPageCommand;
import ua.nure.beizerov.iprovider.web.command.AdminServicesPageCommand;
import ua.nure.beizerov.iprovider.web.command.AdminSubscribersPageCommand;
import ua.nure.beizerov.iprovider.web.command.Command;
import ua.nure.beizerov.iprovider.web.command.LocaleSetCommand;
import ua.nure.beizerov.iprovider.web.command.LoginCommand;
import ua.nure.beizerov.iprovider.web.command.LogoutCommand;
import ua.nure.beizerov.iprovider.web.command.MakeAnOrderCommand;
import ua.nure.beizerov.iprovider.web.command.NoCommand;
import ua.nure.beizerov.iprovider.web.command.ServiceDeleteCommand;
import ua.nure.beizerov.iprovider.web.command.ServiceEditCommand;
import ua.nure.beizerov.iprovider.web.command.ServiceEditPageCommand;
import ua.nure.beizerov.iprovider.web.command.ServiceRegistrationCommand;
import ua.nure.beizerov.iprovider.web.command.ServiceRegistrationPageCommand;
import ua.nure.beizerov.iprovider.web.command.SubscriberHomePageCommand;
import ua.nure.beizerov.iprovider.web.command.SubscriberRegistrationCommand;
import ua.nure.beizerov.iprovider.web.command.SubscriberRegistrationPageCommand;


/**
 * Holder for all commands.<br/>
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class CommandContainer {
	
	public static final String ERROR_MESSAGE = "errorMessage --> ";
	
	// For a locale resource bundle
	public static final String BASE_NAME = "resources";
	
	private static final Logger LOGGER = Logger.getLogger(
			CommandContainer.class
	);
	
	private static Map<String, Command> commands;
	
	
	static {
		commands = Map.ofEntries(
			Map.entry("login", new LoginCommand<>(UserDaoRdbms.getInstance())),
			Map.entry("logout", new LogoutCommand()),
			Map.entry("noCommand", new NoCommand()),
			Map.entry(
				"localeSet",
				new LocaleSetCommand<>(UserDaoRdbms.getInstance())
			),
			
			
			// admin
			Map.entry(
				"goToAdminSubscribersPage",
				new AdminSubscribersPageCommand<>(UserDaoRdbms.getInstance())
			),
			Map.entry(
				"goToAdminServicesPage",
				new AdminServicesPageCommand<>(ServiceDaoRdbms.getInstance())
			),
			
			Map.entry(
				"goToAddSubscriberPage",
				new SubscriberRegistrationPageCommand()
			),
			Map.entry(
				"addSuscriber",
				new SubscriberRegistrationCommand<>(UserDaoRdbms.getInstance())
			),
			
			Map.entry(
				"goToAddServicePage",
				new ServiceRegistrationPageCommand()
			),
			Map.entry(
				"addService",
				new ServiceRegistrationCommand<>(ServiceDaoRdbms.getInstance())
			),
			
			Map.entry(
				"goToServiceEditPage",
				new ServiceEditPageCommand()
			),
			
			Map.entry(
				"activitySwitchAsAdmin",
				new ActivitySwitchAsAdminCommand<>(UserDaoRdbms.getInstance())
			),
			
			Map.entry(
				"serviceDelete",
				new ServiceDeleteCommand<>(ServiceDaoRdbms.getInstance())
			),
			
			Map.entry(
				"serviceEdit",
				new ServiceEditCommand<>(ServiceDaoRdbms.getInstance())
			),
			
			
			// subscriber
			Map.entry(
				"goToSubscriberHomePage",
				new SubscriberHomePageCommand<>(
					AccountDaoRdbms.getInstance(),
					ServiceDaoRdbms.getInstance()
				)
			),
			Map.entry(
				"goToAddServicesPage",
				new AddServicesPageCommand<>(ServiceDaoRdbms.getInstance())
			),
			
			Map.entry(
				"makeAnOrder",
				new MakeAnOrderCommand<>(
					AccountDaoRdbms.getInstance(),
					ServiceDaoRdbms.getInstance(),
					UserDaoRdbms.getInstance()
				)
			)
		);
	}
	
	
	private CommandContainer() {
		throw new IllegalStateException("Utility class");
	}
	
	
	/**
	 * Returns command object with the given name.
	 * 
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(final String commandName) {
		LOGGER.trace("call CommandContainer#get");
		
		if (commandName == null || !commands.containsKey(commandName)) {
			LOGGER.debug(
				"CommandContainer#get(commandName) Command not found, name --> "
				+ commandName
			);
			return commands.get("noCommand"); 
		}
		
		return commands.get(commandName);
	}
}
