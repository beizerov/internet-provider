package ua.nure.beizerov.iprovider.web.command;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Role;
import ua.nure.beizerov.iprovider.model.persistence.dao.UserDao;
import ua.nure.beizerov.iprovider.model.persistence.exception.DaoException;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.web.Path;
import ua.nure.beizerov.iprovider.web.command.util.CommandContainer;


public class AdminSubscribersPageCommand<T extends UserDao> extends Command {

	private static final Logger LOGGER = Logger.getLogger(
			AdminSubscribersPageCommand.class
	);

	private final T userDao;
	
	
	public AdminSubscribersPageCommand(T userDao) {
		if (userDao == null) {
			throw new IllegalArgumentException(
				"userDao argument can't be null"
			);
		}
		
		this.userDao = userDao;
	}
	
	
	@Override
	public String execute(
			HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		LOGGER.trace("AdminSubscribersPageCommand starts");
		
		try {
			request.setAttribute("subscribers", userDao.findByRole(Role.SUB));
		} catch (DaoException | DatabaseException e) {
			LOGGER.error(CommandContainer.ERROR_MESSAGE + e.getMessage(), e);
		}
		
		LOGGER.debug("forward to " + Path.PAGE_ADMIN_SUBSCRIBERS);
		LOGGER.trace("AdminSubscribersPageCommand finished");
		
		return Path.PAGE_ADMIN_SUBSCRIBERS;
	}
}
