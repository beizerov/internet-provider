package ua.nure.beizerov.iprovider.model;


import java.util.Objects;
import java.util.regex.Pattern;

import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents the service model.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class Service extends Entity<Long> {

	private static final long serialVersionUID = -8218668620105329126L;


	private static final Pattern SERVICE_NAME_PATTERN = Pattern.compile(
			"^(?!\\s)[\\p{L}\\p{Nd} ]{2,25}(?<!\\s)$"
	);
	
	
	private String name;
	private float price;
	
	
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of the service.
	 * 
	 * 
	 * Only letters, digits and space. 
	 * Space can't be the first or last character. 
	 * The {@code name} length >= 2 && length <= 25.
	 * 
	 * 
	 * @param name
	 *        The name that matches the regular expression 
	 *        pattern {@code SERVICE_NAME_PATTERN}
	 *        
	 * @throws InvalidDataException If the data doesn't match the pattern
	 */
	public void setName(String name) throws InvalidDataException {
		if (name != null && SERVICE_NAME_PATTERN.matcher(name).find()) {
			this.name = name;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_SERVICE_NAME
			);
		}
	}
	
	public float getPrice() {
		return price;
	}
	
	/**
	 * Sets the price of the service.
	 * 
	 * 
	 * @param price 
	 *        A number in the range 0.0F to 9_999.99F
	 *        
	 * @throws InvalidDataException 
	 *         If the price is less than 0 or greater 9_999.99F
	 */
	public void setPrice(float price) throws InvalidDataException {
		if (price >= 0.0F && price <= 9_999.99F) {
			this.price = price;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_SERVICE_PRICE
			);
		}
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		
		result = prime * result + Objects.hash(name, price);
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!(obj instanceof Service)) {
			return false;
		}
		
		Service other = (Service) obj;
		
		return 
					Objects.equals(name, other.name)
				&& 
					Float.floatToIntBits(price) 
				== 
					Float.floatToIntBits(other.price)
		;
	}

	@Override
	public String toString() {
		return "Service [name=" + name + ", price=" + price + "]";
	}
}
