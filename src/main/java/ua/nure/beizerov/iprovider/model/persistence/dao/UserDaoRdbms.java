package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.Role;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.connection.RdbmsConnectionManager;
import ua.nure.beizerov.iprovider.model.persistence.constant.Fields;
import ua.nure.beizerov.iprovider.model.persistence.dao.mapper.Mapper;
import ua.nure.beizerov.iprovider.model.persistence.exception.DaoException;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents a data access object in the users, and 
 * users_accounts relational tables.
 * Class object is stateless.
 * 
 * The DAO and Singleton (Bill Pugh Singleton Implementation) patterns are used.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class UserDaoRdbms implements UserDao {
	
	private static final Logger LOGGER;
	
	private static final String SQL_CREATE_USER;
	private static final String SQL_FIND_USER_BY_ID;
	private static final String SQL_FIND_USER_BY_LOGIN;
	private static final String SQL_FIND_USERS_BY_ROLE;
	private static final String SQL_UPDATE_USER_ACTIVITY;
	private static final String SQL_UPDATE_USER_LOCALE;
	
	private static final int UNBLOCKED_SUBSCRIBER_BY_DEFAULT = 1;
	
	private final RdbmsConnectionManager connectionManager;
	private final UserMapper userMapper;
	
	static {
		LOGGER = Logger.getLogger(UserDaoRdbms.class);
		
		SQL_CREATE_USER = "INSERT INTO users "
						+ "VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
		
		SQL_FIND_USER_BY_ID = "SELECT * "
				           + " FROM users"
				           + " WHERE user_id = ?";
		
		SQL_FIND_USERS_BY_ROLE = "SELECT * "
				              + "FROM users "
				              + "WHERE role = ?";
		
		SQL_FIND_USER_BY_LOGIN = "SELECT *"
					           + " FROM users "
					           + "WHERE login = ?";
		
		SQL_UPDATE_USER_ACTIVITY = "UPDATE users "
								+ "SET active = ? "
								+ "WHERE user_id = ?";
		
		SQL_UPDATE_USER_LOCALE = "UPDATE users "
								+ "SET locale_language = ? "
								+ "WHERE user_id = ?";
	}
	
	
	private UserDaoRdbms() {
		this.connectionManager = RdbmsConnectionManager.getInstance();
		this.userMapper = new UserMapper();
	}
	
	
	public static UserDaoRdbms getInstance() {
		return SingletonHelper.INSTANCE;
	}
	
	
	private static class SingletonHelper {
		private static final UserDaoRdbms INSTANCE = new UserDaoRdbms();
	}
	
	
	@Override
	public Optional<User> createSubscriber(User user)
			throws DaoException, DatabaseException, InvalidDataException {
		LOGGER.trace("call UserDaoRdbms#createSubscriber(user)");
		
		if (user == null || user.getRole() == Role.ADM) {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_ROLE_FOR_SUBSCRIBER
			);
		}
		
		try (Connection connection = connectionManager.getConnection()) {
			Optional<User> subscriber = Optional.empty();
			
			try {
				connection.setAutoCommit(false);
				
				subscriber = create(connection, user, Role.SUB);
				
				Account account = new Account();
				
				account.setUserId(subscriber.get().getId());
				account.setAccountNumber(UUID.randomUUID().toString());
				account.setBalance(0.0F);
				
				Optional<Account> subscriberAccount = AccountDaoRdbms.create(
					account, connection
				);
				
				if (subscriberAccount.isEmpty()) {
					throw new DaoException(
						InvalidDataExceptionMessages.FAILED_TO_CREATE_ACCOUNT
					);
				}
				
				connection.commit();
				connection.setAutoCommit(true);
				
				return subscriber;
			} catch (DatabaseException | InvalidDataException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				
				LOGGER.error(
					InvalidDataExceptionMessages.FAILED_TO_CREATE_SUBSCRIBE,
					e
				);
				
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_CREATE_SUBSCRIBE,
					e
				);
			}
		} catch (SQLException e) {				
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_USER, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_USER, e
			);				
		} 
	}

	private Optional<User> create(Connection connection, User user, Role role )
			throws DatabaseException {
		LOGGER.trace(
			"call private UserDaoRdbms#create(connection, user, role)"
		);
		
		try (
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setInt(
				parameterIndex++, UNBLOCKED_SUBSCRIBER_BY_DEFAULT
			);
			preparedStatement.setString(parameterIndex++, user.getLogin());
			preparedStatement.setString(parameterIndex++, user.getFirstName());
			preparedStatement.setString(parameterIndex++, user.getLastName());
			preparedStatement.setString(parameterIndex++, user.getPassword());
			preparedStatement.setString(parameterIndex++, role.name());
			preparedStatement.setString(parameterIndex++, user.getLocale());
			
			if (preparedStatement.executeUpdate() <= 0) {
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_CREATE_USER
				);
			}
			
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					user.setId(resultSet.getLong(1));
				}
			}
			
			LOGGER.debug("User was created");
			
			return Optional.ofNullable(user);
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_USER, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_USER, e
			);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 * @throws InvalidDataException 
	 */
	@Override
	public Optional<User> findById(Long id)
			throws DatabaseException {
		LOGGER.trace("call UserDaoRdbms#findById");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_USER_BY_ID
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex++, id);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<User> user = Optional.empty();
				
				if (resultSet.next()) {
					user = Optional.ofNullable(userMapper.mapRow(resultSet));
				}

				LOGGER.debug("UserDaoRdbms#findById ===> with id =  " + id);
				
				return user;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
		}
	}

	@Override
	public List<User> findByRole(Role role)
			throws DatabaseException {
		LOGGER.trace("call UserDaoRdbms#findAll");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_USERS_BY_ROLE
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex++, role.name());
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<User> userList = new ArrayList<>();
				
				while (resultSet.next()) {
					userList.add(userMapper.mapRow(resultSet));
				}
				
				LOGGER.debug(
					"UserDaoRdbms#findByRole#size = " + userList.size()
				);
				
				return userList;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
		}
	}
	
	@Override
	public Optional<User> findByLogin(String login) 
			throws DaoException, DatabaseException {
		LOGGER.trace("call UserDaoRdbms#findByLogin");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_USER_BY_LOGIN
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex++, login);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<User> user = Optional.empty();
				
				if (resultSet.next()) {
					user = Optional.ofNullable(userMapper.mapRow(resultSet));
				}

				LOGGER.debug(
					"UserDaoRdbms#findByLogin ===> with login =  " + login
				);
				
				return user;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_USERS, e
			);
		}
	}

	@Override
	public boolean switchActivity(User user) throws DatabaseException {
		LOGGER.trace("call switchActivity");
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_UPDATE_USER_ACTIVITY
		)) {

			if (user == null) {
				throw new IllegalArgumentException(
					"user parameter can't be null"
				);
			}
			
			int parameterIndex = 1;
			
			preparedStatement.setInt(
				parameterIndex++, user.isActive() ? 0 : 1 
			);
			
			preparedStatement.setLong(parameterIndex++, user.getId());
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_SWITCH_USER_ACTIVITY, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_SWITCH_USER_ACTIVITY, e
			);
		}
	}
	
	@Override
	public boolean switchActivityAsAdmin(User user) throws DatabaseException {
		LOGGER.trace("call switchActivity");
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_UPDATE_USER_ACTIVITY
		)) {

			if (user == null) {
				throw new IllegalArgumentException(
					"user parameter can't be null"
				);
			}
			
			int parameterIndex = 1;
			
			preparedStatement.setInt(
				parameterIndex++, user.isActive() ? -1 : 1
			);
			
			preparedStatement.setLong(parameterIndex++, user.getId());
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_SWITCH_USER_ACTIVITY, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_SWITCH_USER_ACTIVITY, e
			);
		}
	}

	@Override
	public boolean setLocale(User user) throws DatabaseException {
		LOGGER.trace("call setLocale");
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_UPDATE_USER_LOCALE
		)) {

			if (user == null) {
				throw new IllegalArgumentException(
					"user parameter can't be null"
				);
			}
			
			int parameterIndex = 1;
			
			preparedStatement.setString(
				parameterIndex++, user.getLocale()
			);
			
			preparedStatement.setLong(parameterIndex++, user.getId());
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_SET_USER_LOCALE, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_SET_USER_LOCALE, e
			);
		}
	}


	/**
	 * @author Oleksii Beizerov
	 *
	 */
	private static final class UserMapper implements Mapper<User> {

		@Override
		public User mapRow(ResultSet resultSet) throws DatabaseException {
			LOGGER.trace("call UserMapper#mapRow");
	        
	        try {
	        	User user = new User();
	        	
				user.setId(resultSet.getLong(Fields.USER_ID));
		        user.setActive(resultSet.getInt(Fields.ACTIVE));
		        user.setLogin(resultSet.getString(Fields.LOGIN));
		        user.setFirstName(resultSet.getString(Fields.FIRST_NAME));
		        user.setLastName(resultSet.getString(Fields.LAST_NAME));
		        user.setPassword(resultSet.getString(Fields.PASSWORD));
		        user.setRole(Role.valueOf(resultSet.getString(Fields.ROLE)));
		        user.setLocale(resultSet.getString(Fields.LOCALE_LANGUAGE));
		        
		        LOGGER.debug(
		        	"User ==> " + user + " has been mapped"
		        );

		        return user;
			} catch (SQLException | InvalidDataException e) {
				LOGGER.error(
					InvalidDataExceptionMessages.FAILED_TO_MAP_USER, e
				);
				
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_MAP_USER, e
				);
			}
		}
	}
}
