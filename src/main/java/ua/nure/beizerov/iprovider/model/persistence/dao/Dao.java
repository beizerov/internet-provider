package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import ua.nure.beizerov.iprovider.model.Entity;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;


/**
 * <p>
 * 	Defines a common contract for DAO classes that implement this interface.
 * 	Implementing classes must implement CRUD methods.
 * </p>
 * 
 * <p>
 * 	{@code Dao} and the interfaces that extend it separate
 * 	low-level data access logic from business logic.
 * </p>
 *
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> any type expanding {@link Entity}
 * @param <T> id type
 */
public interface Dao<E extends Entity<T>, T extends Serializable> {
	
	/**
	 * Create an entry in persistence system.
	 * 
	 * 
	 * @param entity 
	 *        An object of a class that extends the class Entity
	 * 
	 * @return an optional with an entity that was assigned an id 
	 *         in the database when created
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 * @throws UnsupportedOperationException If call the default implementation
	 */
	default Optional<E> create(E entity) throws DatabaseException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Delete an entry in persistence system by id.
	 * 
	 * 
	 * @param id
	 *        An entry id
	 *        
	 * @return true if entry exists and is successfully deleted, false otherwise
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 * @throws UnsupportedOperationException If call the default implementation
	 */
	default boolean delete(T id) throws DatabaseException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Update an entry in persistence system by entity.
	 * 
	 * 
	 * @param entity
	 *        An object of a class that extends the class Entity
	 *        
	 * @return true if entry exists and is successfully updated, false otherwise
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 * @throws UnsupportedOperationException If call the default implementation
	 */
	default boolean update(E entity) throws DatabaseException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Return entry by id from persistence system as entity.
	 * 
	 * 
	 * @param id
	 * 
	 * @return an optional with entity if an entry with <code>id</code> exists, 
	 *         empty optional otherwise
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 * @throws UnsupportedOperationException If call the default implementation
	 */
	default Optional<E> findById(T id)
			throws DatabaseException {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Return all entries as entities.
	 * 
	 * 
	 * @return list of entities
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 * @throws UnsupportedOperationException If call the default implementation
	 */
	default List<E> findAll() throws DatabaseException {
		throw new UnsupportedOperationException();
	}
}
