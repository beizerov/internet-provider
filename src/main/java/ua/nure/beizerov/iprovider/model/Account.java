package ua.nure.beizerov.iprovider.model;


import java.util.Objects;

import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents the account model.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class Account extends Entity<Long> {

	private static final long serialVersionUID = -7503268502653344303L;
	
	
	private long userId;
	private String accountNumber;
	private float balance;
	
	
	public long getUserId() {
		return userId;
	}
	
	/**
	 * Sets the user id.
	 * 
	 * 
	 * @param userId
	 *        The user id
	 *        
	 * @throws InvalidDataException If the userId is less than 0
	 */
	public void setUserId(long userId) throws InvalidDataException {
		if (userId >= 0) {
			this.userId = userId;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_USER_ID
			);
		}
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}

	
	/**
	 * Sets the account number.
	 * 
	 * 
	 * @param accountNumber
	 *        An account number  (pseudo randomly generated) UUID
	 * 
	 * @throws InvalidDataException
	 *         If the accountNumber is null or accountNumber 
	 *         value length greater then 36
	 */
	public void setAccountNumber(String accountNumber)
			throws InvalidDataException {
		if (accountNumber != null && accountNumber.length() == 36) {
			this.accountNumber = accountNumber;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_ACCOUNT_NUMBER
			);
		}
	}

	public float getBalance() {
		return balance;
	}
	
	/**
	 * Sets the balance.
	 * 
	 * 
	 * @param balance 
	 *        A number in the range -99_999.99F to 99_999.99F
	 *        
	 * @throws InvalidDataException 
	 *         If the balance is less than -99_999.99F or greater 99_999.99F
	 */
	public void setBalance(float balance) throws InvalidDataException {
		if (balance >= -99_999.99F && balance <= 99_999.99F) {
			this.balance = balance;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_BALANCE
			);
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		
		result = prime * result + Objects.hash(balance, userId);
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!(obj instanceof Account)) {
			return false;
		}
		
		Account other = (Account) obj;
		
		return 
					Float.floatToIntBits(balance) 
				== 
					Float.floatToIntBits(other.balance) 
				&& 
					userId == other.userId
		;
	}

	@Override
	public String toString() {
		return "Account [userId=" + userId + ", accountNumber="
				+ accountNumber + ", balance=" + balance + "]"
		;
	}
}
