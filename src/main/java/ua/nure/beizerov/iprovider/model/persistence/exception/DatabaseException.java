package ua.nure.beizerov.iprovider.model.persistence.exception;


import java.sql.SQLException;


/**
 *  An exception that provides information on a database access error.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class DatabaseException extends Exception {

	private static final long serialVersionUID = 7226980971033696255L;

	/**
	 * 
	 */
	public DatabaseException() {
		super();
	}

	/**
	 * @param message
	 */
	public DatabaseException(String message) {
		super(message);
	}

	/**
	 * @param e
	 */
	public DatabaseException(SQLException e) {
		super(e);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}
