package ua.nure.beizerov.iprovider.model;


import java.io.Serializable;
import java.util.Objects;


/**
 * Entity is an abstract superclass for all entities.
 * The entities is used to represent the relationships of a domain model.
 * All subclasses inherit id field and getter and setter for it.
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <T> id type
 */
public abstract class Entity<T extends Serializable> implements Serializable {
	
	private static final long serialVersionUID = -1443416617274268314L;
	
	
	private T id;

	
	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (!(obj instanceof Entity)) {
			return false;
		}
		
		Entity<?> other = (Entity<?>) obj;
		
		return Objects.equals(id, other.id);
	}
}
