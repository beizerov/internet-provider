package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.util.List;
import java.util.Optional;

import ua.nure.beizerov.iprovider.model.Service;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;


/**
 * @author Oleksii Beizerov
 *
 */
public interface ServiceDao extends Dao<Service, Long> {
	
	Optional<Service> findByName(String serviceName)
			throws DatabaseException;
	
	List<Service> findServicesByUserId(long userId)
			throws DatabaseException;
}
