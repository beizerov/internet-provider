package ua.nure.beizerov.iprovider.model;


import java.util.Objects;
import java.util.regex.Pattern;

import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents the user model.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class User extends Entity<Long> {

	private static final long serialVersionUID = -428435241744601880L;
	
	
	private static final Pattern LOGIN_PATTERN = Pattern.compile(
			"^(?=[\\p{IsLatin}\\p{Nd}_!@])(?:[\\p{IsLatin}\\.\\p{Nd}_!@](?<!\\.{2})){6,25}(?<!\\.)$"
	);
	private static final Pattern NAME_PATTERN = Pattern.compile(
			"^\\p{Lu}\\p{Ll}{1,24}$"
	);
	private static final Pattern PASSWORD_PATTERN = Pattern.compile(
			"^(?=[\\p{IsLatin}\\p{Nd}\\p{P}$])(?:[\\p{IsLatin}\\.\\p{Nd}\\p{P}$](?<!\\.{2})){8,50}(?<!\\.)$"
	);
	private static final Pattern LOCALE_PATTERN = Pattern.compile(
			"^\\p{Ll}{2}$"
	);

	
	private int active;
	private String login;
	private String firstName;
	private String lastName;
	private String password;
	private Role role;
	private String locale;
	
	
	public boolean isActive() {
		return active == 1;
	}
	
	public int getActive() {
		return active;
	}
	
	public void setActive(int active) {
		this.active = active;
	}
	
	public String getLogin() {
		return login;
	}
	
	/**
	 * Sets the login.
	 * 
	 * 
	 * Login must be between 6 and 25 characters long. 
	 * Latin letters, numbers, @, !, _ and period. 
	 * A period character cannot be first or last.
	 * {@code login} can't be null.
	 * 
	 * 
	 * @param login
	 *        The String value that matches the regular expression 
	 *        pattern {@code LOGIN_PATTERN}
	 *        
	 * @throws InvalidDataException If the data doesn't match the pattern
	 */
	public void setLogin(String login) throws InvalidDataException {
		if (login != null && LOGIN_PATTERN.matcher(login).find()) {
			this.login = login;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_LOGIN
			);
		}
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets the first name.
	 * 
	 * 
	 * The first letter is uppercase. Minimum 2 and Maximum 25 letters.
	 * {@code firstName} can't be null.
	 * 
	 * 
	 * @param firstName
	 *        The String value that matches the regular expression 
	 *        pattern {@code NAME_PATTERN}
	 *        
	 * @throws InvalidDataException If the data doesn't match the pattern
	 */
	public void setFirstName(String firstName) throws InvalidDataException {
		if (firstName != null && NAME_PATTERN.matcher(firstName).find()) {
			this.firstName = firstName;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_FIRST_NAME
			);
		}
	}
	
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets the last name.
	 * 
	 * 
	 * The first letter is uppercase. Minimum 2 and Maximum 25 letters.
	 * {@code lastName} can't be null.
	 * 
	 * 
	 * @param lastName
	 *        The String value that matches the regular expression 
	 *        pattern {@code NAME_PATTERN}
	 *        
	 * @throws InvalidDataException If the data doesn't match the pattern
	 */
	public void setLastName(String lastName) throws InvalidDataException {
		if (lastName != null && NAME_PATTERN.matcher(lastName).find()) {
			this.lastName = lastName;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_LAST_NAME
			);
		}
	}
	
	public String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 * 
	 * 
	 * {@code password} can't be null and and min length is 8 and max 50.
	 * 
	 * 	<p>
	 *		Characters:
	 *	</p>
	 *	<ul>
	 * 		<li>
	 *   		()[]{}/\_-.,:;*!?@#%"'&$</li> 
	 * 		<li>
	 *   		latin characters
	 *   	</li>
	 *  	<li>
	 *  		decimal digits zero through nine
	 *  	</li>
	 * </ul>
	 * 
	 * 
	 * @param password
	 *        The String value that matches the regular expression 
	 *        pattern {@code PASSWORD_PATTERN}
	 *        
	 * @throws InvalidDataException If the data doesn't match the pattern
	 */
	public void setPassword(String password) throws InvalidDataException {
		if (password != null && PASSWORD_PATTERN.matcher(password).find()) {
			this.password = password;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_PASSWORD
			);
		}
	}
	
	public Role getRole() {
		return role;
	}
	
	/**
	 * Sets the role.
	 * 
	 * 
	 * {@code role} can't be null.
	 * 
	 * 
	 * @param role
	 *        Enumeration value Role
	 *        
	 * @throws InvalidDataException If the locale parameter is null
	 */
	public void setRole(Role role) throws InvalidDataException {
		if (role != null) {
			this.role = role;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_ROLE
			);
		}
	}
	
	public String getLocale() {
		return locale;
	}
	
	/**
	 * Sets the locale.
	 * 
	 * 
	 * @param locale 
	 *        Code for the representation of names of languages in ISO 639-1. 
	 *        
	 * @throws InvalidDataException 
	 *         If the {@code locale} not null or if not,
	 *         {@code locale.length()} not equal 2, or use uppercase
	 */
	public void setLocale(String locale) throws InvalidDataException {
		if (locale == null || LOCALE_PATTERN.matcher(locale).find()) {
			this.locale = locale;
		} else {
			throw new InvalidDataException(
				InvalidDataExceptionMessages.INVALID_LOCALE
			);
		}
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		
		result = prime * result + Objects.hash(
			firstName, lastName, login, password, role
		);
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!(obj instanceof User)) {
			return false;
		}
		
		User other = (User) obj;
		
		return Objects.equals(firstName, other.firstName) 
				&& Objects.equals(lastName, other.lastName)
				&& Objects.equals(login, other.login) 
				&& Objects.equals(password, other.password) 
				&& role == other.role
		;
	}

	@Override
	public String toString() {
		return "User [active=" + active + ", login=" + login 
				+ ", role=" + role + ", locale=" + locale + "]"
		;
	}
}
