package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.persistence.connection.RdbmsConnectionManager;
import ua.nure.beizerov.iprovider.model.persistence.constant.Fields;
import ua.nure.beizerov.iprovider.model.persistence.dao.mapper.Mapper;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents a data access object in the accounts and 
 * accounts_services relational tables.
 * Class object is stateless.
 * 
 * The DAO and Singleton (Bill Pugh Singleton Implementation) patterns are used.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class AccountDaoRdbms implements AccountDao {

	private static final Logger LOGGER;
	
	private static final String SQL_CREATE_ACCOUNT;
	private static final String SQL_FIND_ACCOUNT_BY_USER_ID;
	private static final String SQL_FIND_ACCOUNT_BY_NUMBER;
	private static final String SQL_ADD_SERVICE_BY_ID;
	private static final String SQL_UPDATE_ACCOUNT_BALANCE;
	
	private final RdbmsConnectionManager connectionManager;
	private final AccountMapper accountMapper;
	
	
	static {
		LOGGER = Logger.getLogger(AccountDaoRdbms.class);
		
		SQL_CREATE_ACCOUNT = "INSERT INTO accounts "
		                   + "VALUES (DEFAULT, ?, ?, ?)";
		
		SQL_FIND_ACCOUNT_BY_USER_ID = "SELECT * "
				                   + "FROM accounts "
				                   + "WHERE user_id = ?";
		
		SQL_FIND_ACCOUNT_BY_NUMBER = "SELECT * "
				                  + "FROM accounts "
				                  + "WHERE account_number = ?";
		
		SQL_ADD_SERVICE_BY_ID = "INSERT INTO accounts_services "
				              + "VALUES (?, ?)";
		
		SQL_UPDATE_ACCOUNT_BALANCE = "UPDATE accounts "
									+ "SET balance = ? "
									+ "WHERE account_id = ?";
	}
	
	
	private AccountDaoRdbms() {
		this.connectionManager = RdbmsConnectionManager.getInstance();
		this.accountMapper = new AccountMapper();
	}
	
	
	public static AccountDaoRdbms getInstance() {
		return SingletonHelper.INSTANCE;
	}
	

    private static class SingletonHelper {
        private static final AccountDaoRdbms INSTANCE = new AccountDaoRdbms();
    }
	
	
	/**
	 * Method for creating an account.
	 * 
	 * 
	 * @param account
	 *        An account
	 *        
	 * @param connection
	 *        The connection with Statement.RETURN_GENERATED_KEYS
	 *  
	 * @return an optional with account entity
	 * 
	 * @throws DatabaseException If any SQL error occurs
	 */
	protected static Optional<Account> create(
			Account account, Connection connection
	) throws DatabaseException {
		LOGGER.trace("call AccountDaoRdbms#create(entity, connection)");
		
		try (
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(
				parameterIndex++, account.getUserId()
			);
			preparedStatement.setString(
				parameterIndex++, account.getAccountNumber()
			);
			preparedStatement.setFloat(
				parameterIndex++, account.getBalance()
			);
			
			if (preparedStatement.executeUpdate() == 0) {
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_CREATE_ACCOUNT
				);
			}
			
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					account.setId(resultSet.getLong(1));
				}
			}
			
			LOGGER.debug("Accoun was created");
			
			return Optional.ofNullable(account);
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_ACCOUNT, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_ACCOUNT, e
			);
		}
	}

	
	@Override
	public Optional<Account> findAccountByUserId(long userId)
			throws DatabaseException {
		LOGGER.trace("call AccountDaoRdbms#findAccountByUserId");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_ACCOUNT_BY_USER_ID
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex, userId);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<Account> account = Optional.empty();
				
				if (resultSet.next()) {
					account = Optional.ofNullable(
						accountMapper.mapRow(resultSet)
					);
				}
				
				LOGGER.debug(
					"AccountDaoRdbms#findAccountByUserId ===> with id = " 
					+ userId
				);
				
				return account;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages
					.FAILED_TO_FIND_ACCOUNT_BY_USER_ID,
				e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages
					.FAILED_TO_FIND_ACCOUNT_BY_USER_ID,
				e
			);
		}
	}

	@Override
	public Optional<Account> findAccountByNumber(String accountNumber)
			throws DatabaseException {
		LOGGER.trace("call AccountDaoRdbms#findAccountByNumber");

		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_ACCOUNT_BY_NUMBER
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex, accountNumber);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<Account> account = Optional.empty();
				
				if (resultSet.next()) {
					account = Optional.ofNullable(
						accountMapper.mapRow(resultSet)
					);
				}
				
				LOGGER.debug(
					"AccountDaoRdbms#findAccountByNumber ===> "
				  + "with  accountNumber = " 
					+ accountNumber
				);
				
				return account;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_ACCOUNT_BY_NUMBER, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_ACCOUNT_BY_NUMBER, e
			);
		}
	}

	@Override
	public void addSevicesToAccountById(
			long accountId, long[] arrayOfServiceId
	) throws DatabaseException {
		LOGGER.trace(
			"call AccountDaoRdbms#addSevicesToAccountById(accountId, serviceId)"
		);
		
		try (Connection connection = connectionManager.getConnection()) {
			try {
				connection.setAutoCommit(false);
				
				for (long serviceId : arrayOfServiceId) {
					addSeviceToAccountById(connection, accountId, serviceId);
				}
				
				connection.commit();
				connection.setAutoCommit(true);
			} catch (Exception e) {
				connection.rollback();
				connection.setAutoCommit(true);
				
				LOGGER.error(
					InvalidDataExceptionMessages
						.FAILED_TO_INSERT_INTO_ACCOUNTS_SERVICES
				);
				
				throw new DatabaseException(
					InvalidDataExceptionMessages
						.FAILED_TO_INSERT_INTO_ACCOUNTS_SERVICES,
					e
				);
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages
					.FAILED_TO_INSERT_INTO_ACCOUNTS_SERVICES,
				e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages
					.FAILED_TO_INSERT_INTO_ACCOUNTS_SERVICES,
				e
			);
		}
	}
	
	private static boolean addSeviceToAccountById(
			Connection connection, long accountId, long serviceId
	) throws SQLException {
		LOGGER.trace(
			"call private static AccountDaoRdbms#addUserIdAndAccountId("
					+ "connection, userId, accountId"
			+ ")"
		);
		
		try (
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_ADD_SERVICE_BY_ID
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex++, accountId);
			preparedStatement.setLong(parameterIndex++, serviceId);
			
			return preparedStatement.executeUpdate() > 0;
		}
	}
	
	@Override
	public boolean updateBalance(Account account) throws DatabaseException {
		LOGGER.trace("call updateBalance");
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_UPDATE_ACCOUNT_BALANCE
		)) {

			if (account == null) {
				throw new IllegalArgumentException(
					"account parameter can't be null"
				);
			}
			
			int parameterIndex = 1;
			
			preparedStatement.setFloat(
				parameterIndex++, account.getBalance()
			);
			
			preparedStatement.setLong(parameterIndex++, account.getId());
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_SET_ACCOUNT_BALANCE, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_SET_ACCOUNT_BALANCE, e
			);
		}
	}


	private static final class AccountMapper implements Mapper<Account> {

		@Override
		public Account mapRow(ResultSet resultSet) throws DatabaseException {
			LOGGER.trace("call AccountMapper#mapRow");
			
			try {
				Account account = new Account();

				account.setId(resultSet.getLong(Fields.ACCOUNT_ID));
				account.setUserId(resultSet.getLong(Fields.ACCOUNT_USER_ID));
				account.setAccountNumber(
					resultSet.getString(Fields.ACCOUNT_NUMBER)
				);
				account.setBalance(resultSet.getFloat(Fields.ACCOUNT_BALANCE));
				
		        LOGGER.debug(
		        	"Account with id = " + account.getId() + " has been mapped"
			    );
				
				return account;
			} catch (SQLException | InvalidDataException e) {
				LOGGER.error(
					InvalidDataExceptionMessages.FAILED_TO_MAP_ACCOUNT, e
				);
				
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_MAP_ACCOUNT, e
				);
			}
		}
	}
}
