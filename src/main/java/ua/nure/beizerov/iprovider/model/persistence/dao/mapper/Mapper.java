package ua.nure.beizerov.iprovider.model.persistence.dao.mapper;


import java.sql.ResultSet;

import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;


/**
 * Defines general contract for mapping database result set rows to entities.
 * 
 * <p>
 * Implementations are not supposed to move cursor of the resultSet via next()
 * method, but only extract information from the row in current cursor position.
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 * @param <E> Type for mapping class.
 */
public interface Mapper<E> {
	E mapRow(ResultSet resultSet) throws DatabaseException;
}
