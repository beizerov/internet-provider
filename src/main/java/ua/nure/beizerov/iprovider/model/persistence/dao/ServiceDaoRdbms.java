package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

import ua.nure.beizerov.iprovider.model.Service;
import ua.nure.beizerov.iprovider.model.persistence.connection.RdbmsConnectionManager;
import ua.nure.beizerov.iprovider.model.persistence.constant.Fields;
import ua.nure.beizerov.iprovider.model.persistence.dao.mapper.Mapper;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;
import ua.nure.beizerov.iprovider.model.persistence.exception.constant.InvalidDataExceptionMessages;


/**
 * The class represents a data access object in the services and 
 * accounts_services relational tables.
 * Class object is stateless.
 * 
 * The DAO and Singleton (Bill Pugh Singleton Implementation) patterns are used.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class ServiceDaoRdbms implements ServiceDao {

	private static final Logger LOGGER;
	
	private static final String SQL_CREATE_SERVICE;
	private static final String SQL_DELETE_SERVICE;
	private static final String SQL_UPDATE_SERVICE;
	private static final String SQL_FIND_SERVICE_BY_ID;
	private static final String SQL_FIND_SERVICE_BY_NAME;
	private static final String SQL_FIND_ALL_SERVICES;
	private static final String SQL_FIND_SERVICES_BY_USER_ID;
	
	private final RdbmsConnectionManager connectionManager;
	private final ServiceMapper serviceMapper;
	
	static {
		LOGGER = Logger.getLogger(ServiceDaoRdbms.class);
		
		SQL_CREATE_SERVICE = "INSERT INTO services "
				           + "VALUES (DEFAULT, ?, ?)";
		
		SQL_DELETE_SERVICE = "DELETE FROM services WHERE service_id = ?";
		
		SQL_UPDATE_SERVICE = "UPDATE services "
							+ "SET name = ?, price = ? "
							+ "WHERE service_id = ?";
		
		SQL_FIND_SERVICE_BY_ID = "SELECT * FROM services WHERE service_id = ?";
		
		SQL_FIND_SERVICE_BY_NAME = "SELECT * FROM services WHERE name = ?";
		
		SQL_FIND_ALL_SERVICES = "SELECT * FROM services";
		
		SQL_FIND_SERVICES_BY_USER_ID = "SELECT s.service_id, s.name, s.price "
									+ "FROM services s "
									+ "INNER JOIN accounts_services a_s "
									+ "ON s.service_id = a_s.service_id "
									+ "WHERE a_s.account_id = "
									+ "    (SELECT account_id "
									+ "     FROM accounts "
									+ "     WHERE user_id = ?)";
	}
	
	
	private ServiceDaoRdbms() {
		this.connectionManager = RdbmsConnectionManager.getInstance();
		this.serviceMapper = new ServiceMapper();
	}
	
	
	public static ServiceDaoRdbms getInstance() {
		return SingletonHelper.INSTANCE;
	}
	
	
    private static class SingletonHelper {
        private static final ServiceDaoRdbms INSTANCE = new ServiceDaoRdbms();
    }    

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Service> create(Service service) throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#create");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_CREATE_SERVICE, Statement.RETURN_GENERATED_KEYS
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex++, service.getName());
			preparedStatement.setFloat(parameterIndex++, service.getPrice());
			
			if (preparedStatement.executeUpdate() == 0) {
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_CREATE_SERVICE
				);
			}
			
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					service.setId(resultSet.getLong(1));
				}
			}
			
			LOGGER.debug("Service was created");
			
			return Optional.ofNullable(service);
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_SERVICE, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_CREATE_SERVICE, e
			);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean delete(Long id) throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#delete");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_DELETE_SERVICE
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex++, id);
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_DELETE_SERVICE, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_DELETE_SERVICE, e
			);
		}
	}

	@Override
	public boolean update(Service service) throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#update");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_UPDATE_SERVICE
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex++, service.getName());
			preparedStatement.setFloat(parameterIndex++, service.getPrice());
			preparedStatement.setLong(parameterIndex++, service.getId());
			
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_UPDATE_SERVICE, e
			);
				
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_UPDATE_SERVICE, e
			);
		}
	}
	
	@Override
	public Optional<Service> findById(Long id) throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#findById");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_SERVICE_BY_ID
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex++, id);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<Service> service = Optional.empty();
				
				if (resultSet.next()) {
					service = Optional.ofNullable(
						serviceMapper.mapRow(resultSet)
					);
				}
				
				LOGGER.debug(
					"ServiceDaoRdbms#findById ===> with id = " + id
				);
				
				return service;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
		}
	}
	
	@Override
	public Optional<Service> findByName(String serviceName) throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#findByName");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_SERVICE_BY_NAME
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setString(parameterIndex++, serviceName);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				Optional<Service> service = Optional.empty();
				
				if (resultSet.next()) {
					service = Optional.ofNullable(
						serviceMapper.mapRow(resultSet)
					);
				}
				
				LOGGER.debug(
					"ServiceDaoRdbms#findByName ===> with name = " + serviceName
				);
				
				return service;
			}
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
		}
	}
	
	@Override
	public List<Service> findServicesByUserId(long usertId)
			throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#findServicesByUserId");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_SERVICES_BY_USER_ID
		)) {
			int parameterIndex = 1;
			
			preparedStatement.setLong(parameterIndex++, usertId);

			List<Service> serviceList = new ArrayList<>();
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					serviceList.add(serviceMapper.mapRow(resultSet));
				}
			}
			
			LOGGER.debug(
				"ServiceDaoRdbms#findServicesByUserId#size = " 
				+ serviceList.size()
			);
			
			return serviceList;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
		}
	}

	@Override
	public List<Service> findAll() throws DatabaseException {
		LOGGER.trace("call ServiceDaoRdbms#findAll");
		
		try (
			Connection connection = connectionManager.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
				SQL_FIND_ALL_SERVICES
			);
			ResultSet resultSet = preparedStatement.executeQuery()
		) {
			List<Service> serviceList = new ArrayList<>();
			
			while (resultSet.next()) {
				serviceList.add(serviceMapper.mapRow(resultSet));
			}
			
			LOGGER.debug(
				"ServiceDaoRdbms#findAll#size = " + serviceList.size()
			);
			
			return serviceList;
		} catch (SQLException e) {
			LOGGER.error(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
			
			throw new DatabaseException(
				InvalidDataExceptionMessages.FAILED_TO_FIND_SERVICES, e
			);
		}
	}


	private static final class ServiceMapper implements Mapper<Service> {

		@Override
		public Service mapRow(ResultSet resultSet) throws DatabaseException {
			LOGGER.trace("call ServiceMapper#mapRow");
			
			try {
				Service service = new Service();
				
				service.setId(
					resultSet.getLong(Fields.SERVICE_ID)
				);
				service.setName(
					resultSet.getString(Fields.SERVICE_NAME)
				);
				service.setPrice(
					resultSet.getFloat(Fields.SERVICE_PRICE)
				);
				
				LOGGER.debug(
					"Service with id " + service.getId() + " has been mapped"
				);
				
				return service;
			} catch (SQLException | InvalidDataException e) {
				LOGGER.error(
					InvalidDataExceptionMessages.FAILED_TO_MAP_SERVICE, e
				);
				
				throw new DatabaseException(
					InvalidDataExceptionMessages.FAILED_TO_MAP_SERVICE, e
				);
			}
		}
	}
}
