package ua.nure.beizerov.iprovider.model.persistence.connection;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;


/**
 * This class is used to connect to a relational database 
 * using a connection pool. 
 * The WEB_APP_ROOT/META-INF/context.xml file is used for configuration.
 *  
 * The Singleton pattern is used (Bill Pugh's implementation of the Singleton).
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class RdbmsConnectionManager {

	private static final Logger LOGGER = Logger.getLogger(RdbmsConnectionManager.class);

	private DataSource dataSource;

	
	private RdbmsConnectionManager() {
		setDataSource();
	}

	
	public static RdbmsConnectionManager getInstance() {
		return SingletonHelper.INSTANCE;
	}

	
    private static class SingletonHelper {
        private static final 
        RdbmsConnectionManager INSTANCE = new RdbmsConnectionManager();
    }
	
	
	/**
	 * Returns a DB connection from the Pool Connections. 
	 * 
	 * @return A DB connection.
	 */
	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	
	/**
	 * Configures {@code DataSource}. Sets the data source configuration via JNDI.
	 * This method should only be called once.
	 * 
	 * 	<pre>
	 * {@code 
	 *  Context initialContext = new InitialContext();
	 *  Context envContext = (Context) initialContext.lookup("java:/comp/env");
	 *  dataSource = (DataSource) envContext.lookup("jdbc/internetProviderDB");
	 * }
	 * </pre>
	 * 
	 */
	private void setDataSource() {
		if (dataSource == null) {

			try {
				Context initialContext = new InitialContext();
				Context envContext = (Context) initialContext.lookup(
					"java:/comp/env"
				);
				dataSource = (DataSource) envContext.lookup(
					"jdbc/internetProviderDB"
				);
			} catch (NamingException e) {
				LOGGER.error(e);
			}
		} else {
			LOGGER.debug("The Datasource is already configured");
		}

		LOGGER.trace("Data source ==> " + dataSource);
	}
}
