package ua.nure.beizerov.iprovider.model.persistence.exception;


/**
 * @author Oleksii Beizerov
 *
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = -9195090387875823802L;
	

	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
