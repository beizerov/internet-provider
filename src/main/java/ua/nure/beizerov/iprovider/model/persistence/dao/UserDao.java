package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.util.List;
import java.util.Optional;

import ua.nure.beizerov.iprovider.model.Role;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.exception.DaoException;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;


/**
 * @author Oleksii Beizerov
 *
 */
public interface UserDao extends Dao<User, Long> {

	Optional<User> createSubscriber(User user)
			throws DaoException, DatabaseException, InvalidDataException;
	
	List<User> findByRole(Role role)
			throws DaoException, DatabaseException;
	
	Optional<User> findByLogin(String login)
			throws DaoException, DatabaseException;
	
	boolean switchActivity(User user)
			throws DatabaseException;
	
	boolean switchActivityAsAdmin(User user)
			throws DatabaseException;
	
	boolean setLocale(User user)
			throws DatabaseException;
}
