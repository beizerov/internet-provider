package ua.nure.beizerov.iprovider.model.persistence.constant;


/**
 * The class contains {@code String} constants of the names of the fields of 
 * the database tables.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public final class Fields {
	
	private Fields() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	// users table
	public static final String USER_ID = "user_id";
	public static final String ACTIVE = "active";
	public static final String LOGIN = "login";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String PASSWORD = "password";
	public static final String ROLE = "role";
	public static final String LOCALE_LANGUAGE = "locale_language";
	
	// services table
	public static final String SERVICE_ID = "service_id";
	public static final String SERVICE_NAME = "name";
	public static final String SERVICE_PRICE = "price";
	
	// accounts table
	public static final String ACCOUNT_ID = "account_id";
	public static final String ACCOUNT_USER_ID = "user_id";
	public static final String ACCOUNT_NUMBER = "account_number";
	public static final String ACCOUNT_BALANCE = "balance";
}
