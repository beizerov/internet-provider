package ua.nure.beizerov.iprovider.model.persistence.dao;


import java.util.Optional;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.persistence.exception.DatabaseException;


public interface AccountDao extends Dao<Account, Long> {

	Optional<Account> findAccountByUserId(long userId) 
			throws DatabaseException;
	
	Optional<Account> findAccountByNumber(String accountNumber) 
			throws DatabaseException;
	
	void addSevicesToAccountById(long accountId, long[] arrayOfServiceId) 
			throws DatabaseException;
	
	boolean updateBalance(Account account) 
			throws DatabaseException;
}
