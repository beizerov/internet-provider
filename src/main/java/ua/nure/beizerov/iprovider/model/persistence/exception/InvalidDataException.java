package ua.nure.beizerov.iprovider.model.persistence.exception;


/**
 * An exception throw an error when data is entered invalid.
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public class InvalidDataException extends Exception {

	private static final long serialVersionUID = 1001930450890324045L;

	
	public InvalidDataException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public InvalidDataException(String message) {
		super(message);
	}
}
