package ua.nure.beizerov.iprovider.model.persistence.exception.constant;


public final class InvalidDataExceptionMessages {
	
	private InvalidDataExceptionMessages() {
		throw new IllegalStateException("Do not create instances!");
	}
	
	
	// users
	public static final 
	String INVALID_LOGIN = "Login does not match the pattern";
	
	public static final 
	String INVALID_ROLE_FOR_SUBSCRIBER = "Subscriber can't have admin role";
	
	public static final
	String INVALID_FIRST_NAME = "First name does not match the pattern";
	
	public static final 
	String INVALID_LAST_NAME = "Last name does not match the pattern";
	
	public static final
	String INVALID_PASSWORD = "Password does not match the pattern";
	
	public static final
	String INVALID_ROLE = "Role can't be null";
	
	public static final
	String INVALID_LOCALE = "Locale can be null or code length must be 2 chars";

	public static final
	String FAILED_TO_MAP_USER = "Failed to map user";
	
	public static final
	String FAILED_TO_CREATE_USER = "Failed to create user";
	public static final
	String FAILED_TO_CREATE_SUBSCRIBE = "Subscriber not created, transaction rollback";
	
	public static final
	String FAILED_TO_FIND_USER_BY_ID = "Failed to find user by id";
	
	public static final
	String FAILED_TO_FIND_USERS = "Failed to find users";
	
	public static final
	String FAILED_TO_SWITCH_USER_ACTIVITY = "Failed to switch user activity";

	public static final
	String FAILED_TO_SET_USER_LOCALE = "Failed to set user locale";
	
	
	// services
	public static final 
	String INVALID_SERVICE_NAME = "Service name does not match the pattern";
	
	public static final
	String INVALID_SERVICE_PRICE = "Service price is out of range";
	
	public static final
	String FAILED_TO_MAP_SERVICE = "Faild to map service";
	
	public static final
	String FAILED_TO_CREATE_SERVICE = "Failed to create service";
	
	public static final
	String FAILED_TO_DELETE_SERVICE = "Failed to delete service";
	public static final
	String FAILED_TO_UPDATE_SERVICE = "Failed to update service";
	
	public static final
	String FAILED_TO_FIND_SERVICES = "Failed to find services";
	
	
	// accounts
	public static final 
	String INVALID_USER_ID = "User id can't be less then 0";
	
	public static final 
	String INVALID_ACCOUNT_NUMBER = "Account number can't be null or accountNumber value length greater then 36";
	
	public static final
	String INVALID_BALANCE = "Balance is out of range";
	
	public static final
	String FAILED_TO_MAP_ACCOUNT = "Faild to map account";
	
	public static final
	String FAILED_TO_CREATE_ACCOUNT = "Failed to create account";
	
	public static final
	String FAILED_TO_FIND_ACCOUNTS = "Failed to find accounts";
	
	public static final
	String FAILED_TO_FIND_ACCOUNT_BY_USER_ID = "Failed to find account by user id";
	
	public static final
	String FAILED_TO_FIND_ACCOUNT_BY_NUMBER = "Failed to find account by number";

	public static final
	String FAILED_TO_SET_ACCOUNT_BALANCE = "Failed to set account balance";
	
	// accounts_services
	public static final
	String FAILED_TO_INSERT_INTO_ACCOUNTS_SERVICES = "Failed to insert account id and service id into accounts_services table";
	
	// UserDaoRdbms
	public static final
	String INVALID_SUBSCRIBER = "Subscriber can't be null or be an admin";
}
