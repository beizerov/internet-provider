package ua.nure.beizerov.iprovider.model;


/**
 * This enumeration represents all valid roles in the application.
 * 
 * <p>
 * 	ADM is admin
 * 	SUB is subscriber
 * </p>
 * 
 * 
 * @author Oleksii Beizerov
 *
 */
public enum Role {
	ADM, SUB
}
