package ua.nure.beizerov.iprovider.exception;


/**
 * @author Oleksii Beizerov
 *
 */
public class ApplicationRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -5477790924227240731L;

	
	public ApplicationRuntimeException() {
	}

	public ApplicationRuntimeException(String message) {
		super(message);
	}

	public ApplicationRuntimeException(Throwable cause) {
		super(cause);
	}

	public ApplicationRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
