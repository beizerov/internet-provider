DROP DATABASE IF EXISTS `db-for-internet-provider`;


CREATE DATABASE `db-for-internet-provider`
CHARACTER SET = utf8mb4 
COLLATE = utf8mb4_unicode_ci;

USE `db-for-internet-provider`;


CREATE TABLE `users` (
	`user_id` INT UNSIGNED AUTO_INCREMENT,
	`active` INT DEFAULT 0,
	`login` VARCHAR(25) CHARACTER SET ascii BINARY UNIQUE NOT NULL ,
	`first_name` VARCHAR(25) NOT NULL,
	`last_name` VARCHAR(25) NOT NULL,
	`password` VARCHAR(50) CHARACTER SET ascii BINARY NOT NULL,
	`role` VARCHAR(3) NOT NULL,
	`locale_language` CHAR(2),	-- Use ISO 639-1 for locale
	
	
	PRIMARY KEY (`user_id`),
	
	CONSTRAINT c1_key_users_login CHECK (
		LENGTH(`login`) >= 6
	),
	
	CONSTRAINT c2_key_users_first_name CHECK (
		LENGTH(`first_name`) >= 2
	),
	
	CONSTRAINT c3_key_users_last_name CHECK (
		LENGTH(`last_name`) >= 2
	),	
	
	CONSTRAINT c4_key_users_password CHECK (
		LENGTH(`password`) >= 8
	),
	
	CONSTRAINT c5_key_users_role CHECK (
		`role` IN ('ADM', 'SUB')
	)
);

CREATE TABLE `services` (
	`service_id` INT UNSIGNED AUTO_INCREMENT,
	`name` VARCHAR(25) BINARY UNIQUE NOT NULL,
	`price` DECIMAL(6, 2) UNSIGNED NOT NULL,
	
	
	PRIMARY KEY (`service_id`),
	
	CONSTRAINT c1_key_services_name CHECK (
		LENGTH(`name`) >= 2
	)
);

CREATE TABLE `accounts` (
	`account_id` INT UNSIGNED AUTO_INCREMENT,
	`user_id` INT UNSIGNED UNIQUE,
	`account_number` VARCHAR(36) CHARACTER SET ascii UNIQUE NOT NULL,
	`balance` DECIMAL(7, 2) NOT NULL,
	
	
	PRIMARY KEY (`account_id`),
	
	CONSTRAINT c1_key_accounts_account_number CHECK (
		LENGTH(`account_number`) = 36
	),
	
	CONSTRAINT f1_key_accounts_user_id FOREIGN KEY (`user_id`) 
	REFERENCES `users` (`user_id`) ON DELETE CASCADE
);

CREATE TABLE `accounts_services` (
	`account_id` INT UNSIGNED,
	`service_id` INT UNSIGNED,
	
	
	PRIMARY KEY (`account_id`, `service_id`),
	
	CONSTRAINT f1_key_accounts_service_account_id FOREIGN KEY (`account_id`) 
	REFERENCES `accounts` (`account_id`) ON DELETE CASCADE,
	
	CONSTRAINT f2_key_accounts_service_service_id FOREIGN KEY (`service_id`)
	REFERENCES `services` (`service_id`) ON DELETE CASCADE
);


-- insert admin for test
INSERT INTO `users` 
VALUES (DEFAULT, 1, '@dm!n!str@t0r', 'Alex', 'Alex', '@d/\\/\\!n!$tr@t0r', 'ADM', NULL);
