package ua.nure.beizerov.iprovider.persistence.model;


import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ua.nure.beizerov.iprovider.model.Account;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;


class AccountTest {
	
	private static Account account; 
	private static String uuid;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		uuid = UUID.randomUUID().toString();
		
		account = new Account();
		
		account.setId(1L);
		account.setAccountNumber(uuid);
		account.setUserId(1L);
		account.setBalance(99.99F);
	}

	@Test
	final void testHashCode() {
		assertEquals(account.hashCode(), account.hashCode());
	}

	@Test
	final void testEqualsObject() {
		assertEquals(account, account);
	}

	@Test
	final void testGetAccountNumber() {
		assertEquals(uuid, account.getAccountNumber());
	}
	
	@Test
	final void testSetAccountNumber() {
		assertDoesNotThrow(() -> account.setAccountNumber(uuid));
	}

	@Test
	final void testGetBalance() {
		assertEquals(99.99F, account.getBalance());
	}

	@Test
	final void testSetBalance() {
		assertThrows(InvalidDataException.class, () -> account.setBalance(-100_000F));
	}
}
