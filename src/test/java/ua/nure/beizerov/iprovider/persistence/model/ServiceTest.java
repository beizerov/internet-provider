package ua.nure.beizerov.iprovider.persistence.model;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ua.nure.beizerov.iprovider.model.Service;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;


class ServiceTest {
	
	private static Service service;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		service = new Service();
		
		service.setId(1L);
		service.setName("Internet");
		service.setPrice(150.0F);
	}

	@Test
	final void testHashCode() {
		assertEquals(service.hashCode(), service.hashCode());
	}

	@Test
	final void testEqualsObject() {
		assertEquals(service, service);
	}

	@Test
	final void testGetName() {
		assertEquals("Internet", service.getName());
	}

	@Test
	final void testSetName() {
		assertThrows(InvalidDataException.class, () -> service.setName(null));
	}

	@Test
	final void testGetPrice() {
		assertEquals(150.0F, service.getPrice());
	}

	@Test
	final void testSetPrice() {
		assertThrows(InvalidDataException.class, () -> service.setPrice(-1F));
	}
}
