package ua.nure.beizerov.iprovider.persistence.model;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ua.nure.beizerov.iprovider.model.Role;
import ua.nure.beizerov.iprovider.model.User;
import ua.nure.beizerov.iprovider.model.persistence.exception.InvalidDataException;


class UserTest {

	private static User user;
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		user = new User();
		
		user.setId(1L);
		user.setActive(1);
		user.setLogin("login1");
		user.setFirstName("Fame");
		user.setLastName("Game");
		user.setPassword("password");
		user.setRole(Role.SUB);
		user.setLocale(null);
	}


	@Test
	final void testHashCode() {
		assertEquals(user.hashCode(), user.hashCode());
	}

	@Test
	final void testEqualsObject() {
		assertEquals(user, user);
	}

	@Test
	final void testUser() {
		assertNotNull(user);
	}

	@Test
	final void testIsActive() {
		assertTrue(user.isActive());
	}

	@Test
	final void testGetActive() {
		assertTrue(((Integer) user.getActive()) instanceof Integer);
	}

	@Test
	final void testSetActive() {
		user.setActive(0);
		
		assertEquals(0, user.getActive());
		
		user.setActive(1);
	}

	@Test
	final void testGetLogin() {
		assertEquals("login1", user.getLogin());
	}

	@Test
	final void testSetLogin() {
		assertDoesNotThrow(() -> user.setLogin("login2"));
	}

	@Test
	final void testGetFirstName() {
		assertEquals("Fames", user.getFirstName());
	}

	@Test
	final void testSetFirstName() {
		assertDoesNotThrow(() -> user.setFirstName("Fames"));
	}

	@Test
	final void testGetLastName() {
		assertEquals("Games", user.getLastName());
	}

	@Test
	final void testSetLastName() {
		assertDoesNotThrow(() -> user.setLastName("Games"));
	}

	@Test
	final void testGetPassword() {
		assertEquals("password", user.getPassword());
	}

	@Test
	final void testSetPassword() {
		assertThrows(InvalidDataException.class, () -> user.setPassword(null));
	}

	@Test
	final void testGetRole() {
		assertEquals(Role.SUB, user.getRole());
	}

	@Test
	final void testSetRole() {
		assertThrows(InvalidDataException.class, () -> user.setRole(null));
	}

	@Test
	final void testGetLocale() {
		assertNotNull(user.getLocale());
	}

	@Test
	final void testSetLocale() throws InvalidDataException {
		user.setLocale("en");
		
		assertEquals("en", user.getLocale());
	}

	@Test
	final void testGetId() {
		assertNotNull(user.getId());
	}

	@Test
	final void testSetId() {
		user.setId(5L);
		
		assertEquals(5L, user.getId());
	}
}
