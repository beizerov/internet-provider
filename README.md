# INTERNET PROVIDER


##### [Task](https://gitlab.com/beizerov/internet-provider/blob/master/TASK.md)




##### JAVA VERSION 11

##### [HikariCP](https://github.com/brettwooldridge/HikariCP) connection pool is used to connect to the database

Use the WEB_APP_ROOT/META-INF/context.xml file to configure DBCP.

##### EXAMPLE
```
<Context>
	<Resource 
		name="jdbc/internetProviderDB" 
		auth="Container"
		factory="com.zaxxer.hikari.HikariJNDIFactory"
		type="javax.sql.DataSource" 
		maximumPoolSize="100"
		transactionIsolation="TRANSACTION_READ_COMMITTED"
		dataSourceClassName="com.mysql.cj.jdbc.MysqlDataSource"
		dataSource.url="jdbc:mysql://172.17.0.2:3306/db-for-internet-provider?useSSL=true"
		dataSource.user="admin" 
		dataSource.password="I_am_admin"
	/>
</Context>

```
##### MySQL database